class AppEndpoint {
  static const String BASE = "https://kingbuy.sapp.asia";

  /// ?limit=20&offset=0
  static const String PROMOTION = "/api/getAllPromotion";

  static const String HOT_CATEGORY = "/api/getHotCategories";

  /// ?limit=20&offset=0
  static const String MY_PROMOTION = "/api/getAllMyPromotion";

  /// ?limit=20&offset=0
  static const String PRODUCT_NEW = "/api/getAllProductNew";

  /// ?limit=20&offset=0
  static const String PRODUCT_SELLING = "/api/getAllProductSelling";

  /// ?limit=20&offset=0
  static const String ALL_CATEGORY = "/api/getAllCategories";

  /// param {
  ///   product_category_id
  ///   searchWord
  ///   offset
  ///   limit
  ///   brand_id
  ///   price
  ///   from
  ///   to
  /// }
  static const String PRODUCTS_BY_CATEGORY = "/api/getProductsByCategory";

  /// param {
  ///   searchWord
  ///   offset
  ///   limit
  ///   brand_id
  ///   from
  ///   to
  /// }
  static const String SEARCH_PRODUCT = "/api/searchProduct";

  static const String COMMITMENTS = "/api/getCommitments";

  static const String CONTACT = "/api/contactUs";

  static const String RATING = "/api/ratingInfoByProduct";

  /// param {
  ///   product_id
  /// }
  static const String REVIEW = "/api/getReviewByProduct";

  static const String ADDRESS = "/api/getAllAddress";

  static const String PRODUCT_QUESTIONS = "/api/getProductQuestions";

  static const String PURCHAR_TOGETHER = "/api/purchasedTogetherProducts";

  static const String RELATED = "/api/relatedProduct/";

  /// param {
  ///   identity
  ///   password
  /// }
  static const String LOGIN = "/api/loginApp";

  /// {}
  static const String MY_COUPONS = "/api/getMyCoupons";

  static const String NOTIFICATION = "/api/listNotification";

  static const String CITY = "/api/getAllProvince";

  /// {
  ///   /{cityCode}
  /// }
  static const String DISTRICT = "/api/getDistrictByProvinceCode/";

  /// {
  ///   /{districtCode}
  /// }
  static const String WARD = "/api/getWardByDistrictCode/";
}
