import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:hues_kingbuy/src/config/app_endpoint.dart';
import 'package:hues_kingbuy/src/resource/model/coupon.dart';
import 'package:hues_kingbuy/src/resource/repository/promotion_repository.dart';
import 'package:hues_kingbuy/src/screen/cart/cart.dart';
import 'package:hues_kingbuy/src/utils/custom_format.dart';
import 'package:hues_kingbuy/src/utils/shared_pref.dart';
import 'package:provider/provider.dart';

class CartCouponScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final cartVM = Provider.of<CartViewmodel>(context, listen: true);
    return Scaffold(
      appBar: AppBar(
        title: Text('Mã giảm giá'),
      ),
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.all(10),
              child: Text(
                'Chọn 1 mã giảm giá',
                style: TextStyle(
                  color: Colors.black87,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
            FutureBuilder<Coupon>(
              future: PromotionRepository().getMyCoupon(SharedPref.getToken()),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return Center(child: CircularProgressIndicator());
                } else {
                  return ListView.builder(
                    shrinkWrap: true,
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    itemCount: snapshot.data.data.nonUse.rows.length,
                    itemBuilder: (context, index) {
                      final item = snapshot.data.data.nonUse.rows[index];
                      return GestureDetector(
                        onTap: cartVM.total < item.invoiceTotal
                            ? null
                            : () => _change(cartVM, item),
                        behavior: HitTestBehavior.opaque,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            CachedNetworkImage(
                              imageUrl: AppEndpoint.BASE + item.imageSource,
                              width: 140,
                            ),
                            Flexible(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                mainAxisSize: MainAxisSize.max,
                                children: [
                                  Text(item.name),
                                  SizedBox(height: 10),
                                  Text('HDS: ${item.expiresAt}'),
                                  SizedBox(height: 6),
                                  cartVM.total < item.invoiceTotal
                                      ? Text(
                                          'Vouche không đủ điều kiện,'
                                          ' đơn hàng ít nhất'
                                          ' ${CustomFormat.formatMoney(item.invoiceTotal)}',
                                          style: TextStyle(
                                            color: Colors.red.shade700,
                                            fontWeight: FontWeight.w500,
                                          ),
                                        )
                                      : Container(),
                                ],
                              ),
                            ),
                            Checkbox(
                              value: cartVM.optionCoupon == null
                                  ? false
                                  : cartVM.optionCoupon.id == item.id,
                              onChanged: (value) {},
                              activeColor: Colors.red.shade700,
                            ),
                          ],
                        ),
                      );
                    },
                  );
                }
              },
            ),
          ],
        ),
      ),
    );
  }

  _change(CartViewmodel vm, CouponItem item) {
    if (vm.optionCoupon == null) {
      vm.optionCoupon = item;
    } else {
      if (vm.optionCoupon.id == item.id) {
        vm.optionCoupon = null;
      } else {
        vm.optionCoupon = item;
      }
    }
  }
}
