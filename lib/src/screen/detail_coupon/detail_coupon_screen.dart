import 'package:flutter/material.dart';
import 'package:hues_kingbuy/src/resource/model/model.dart';
import 'package:hues_kingbuy/src/screen/widget/widget.dart';

class DetailCouponScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Map arguments = ModalRoute.of(context).settings.arguments;
    final Coupon coupon = arguments["coupon"];

    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.red.shade700,
          title: Text("Coupon của tôi"),
          bottom: TabBar(
            tabs: [
              Tab(text: "Chưa sử dụng"),
              Tab(text: "Đã sử dụng"),
              Tab(text: "Đã hết hạn"),
            ],
          ),
        ),
        body: TabBarView(
          children: [
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: coupon.data.nonUse.count == 0
                  ? Center(child: Text("Không có coupon nào cả"))
                  : ListView.builder(
                      itemCount: coupon.data.nonUse.count,
                      itemBuilder: (context, index) {
                        return ItemCoupon(item: coupon.data.nonUse.rows[index]);
                      },
                    ),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: coupon.data.used.count == 0
                  ? Center(child: Text("Không có coupon nào cả"))
                  : ListView.builder(
                      itemCount: coupon.data.used.count,
                      itemBuilder: (context, index) {
                        return ItemCoupon(item: coupon.data.used.rows[index]);
                      },
                    ),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: coupon.data.expired.count == 0
                  ? Center(child: Text("Không có coupon nào cả"))
                  : ListView.builder(
                      itemCount: coupon.data.expired.count,
                      itemBuilder: (context, index) {
                        return ItemCoupon(
                            item: coupon.data.expired.rows[index]);
                      },
                    ),
            ),
          ],
        ),
      ),
    );
  }
}
