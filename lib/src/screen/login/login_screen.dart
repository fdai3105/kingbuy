import 'package:flutter/material.dart';
import 'package:hues_kingbuy/src/screen/login/login.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    String identity;
    String password;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Column(
              children: [
                Image.asset(
                  "assets/icons/logo.png",
                  width: 140,
                ),
                Text(
                  "Đăng nhập",
                  style: TextStyle(
                    color: Colors.black87,
                    fontSize: 20,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                SizedBox(height: 40),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 40),
                  child: Column(
                    children: [
                      LoginField(
                        onTap: (value) {
                          identity = value;
                        },
                        hint: "Email hoặc SĐT",
                        icon: Icons.person,
                      ),
                      SizedBox(height: 20),
                      LoginField(
                        onTap: (value) {
                          password = value;
                        },
                        hint: "Mật khẩu",
                        icon: Icons.lock,
                      ),
                      SizedBox(height: 22),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          FlatButton(
                            onPressed: () {},
                            child: Text("Quên mật khẩu?"),
                            padding: EdgeInsets.symmetric(
                              horizontal: 30,
                              vertical: 20,
                            ),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20),
                            ),
                          ),
                          FlatButton(
                            onPressed: () {
                              final provider = Provider.of<LoginViewmodel>(
                                  context,
                                  listen: false)
                                ..setContext(context);
                              provider
                                  .onLogin(identity, password)
                                  .then((value) {
                                if (value == LoginStatus.FAIL) {
                                  print('fail');
                                } else {
                                  print('success');
                                }
                                Navigator.pop(context);
                              });
                            },
                            child: Text("Đăng nhập"),
                            padding: EdgeInsets.symmetric(
                              horizontal: 30,
                              vertical: 20,
                            ),
                            color: Colors.red.shade700,
                            textColor: Colors.white,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Column(
              children: [
                Text(
                  "Hoặc đăng nhập bằng",
                  style: TextStyle(
                    color: Colors.black87,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 14),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        "assets/icons/logo_face.png",
                        height: 40,
                        width: 40,
                      ),
                      SizedBox(width: 20),
                      Image.asset(
                        "assets/icons/logo_google.png",
                        height: 40,
                        width: 40,
                      ),
                    ],
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "không có tài khoản? ",
                      style: TextStyle(
                        color: Colors.black87,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    GestureDetector(
                      onTap: () {},
                      child: Text(
                        "Đăng ký",
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                          decoration: TextDecoration.underline,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}

class LoginField extends StatelessWidget {
  final Function(String) onTap;
  final String hint;
  final IconData icon;

  const LoginField({
    Key key,
    this.hint,
    this.icon,
    @required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(30),
          border: Border.all(color: Colors.black54),
          boxShadow: [
            BoxShadow(
              color: Colors.black26,
              offset: Offset(4, 4),
              blurRadius: 6,
            )
          ]),
      child: TextField(
        onChanged: onTap,
        decoration: InputDecoration(
          icon: Icon(icon, color: Colors.grey),
          focusColor: Colors.red,
          hintText: hint ?? "",
          border: InputBorder.none,
        ),
      ),
    );
  }
}
