import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hues_kingbuy/src/resource/model/model.dart';
import 'package:hues_kingbuy/src/resource/repository/user_repository.dart';
import 'package:hues_kingbuy/src/screen/base/base_viewmodel.dart';
import 'package:hues_kingbuy/src/utils/route.dart';
import 'package:hues_kingbuy/src/utils/shared_pref.dart';
import 'package:hues_kingbuy/src/utils/utils.dart';

enum LoginStatus {
  LOADING,
  FAIL,
  SUCCESS,
}

class LoginViewmodel extends BaseViewmodel {
  final UserRepository userRepository;

  LoginViewmodel(this.userRepository);

  Future<LoginStatus> onLogin(String identity, String password) async {
    _showDialog(context);
    LoginStatus status;
    final User response = await userRepository.login(identity, password);
    if (response == null) {
      status = LoginStatus.FAIL;
      Navigator.pop(context);
    } else {
      status = LoginStatus.SUCCESS;
      await SharedPref.saveUser(response);
      Navigator.maybePop(context).then(
          (value) => Navigator.pushNamed(context, Routes.dashboardScreen));
    }
    return status;
  }

  _showDialog(BuildContext context) {
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (context) {
        return WillPopScope(
          onWillPop: () async => false,
          child: Dialog(
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CircularProgressIndicator(),
                  SizedBox(width: 10),
                  Text("Đang đăng nhập"),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
