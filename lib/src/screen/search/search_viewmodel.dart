import 'package:hues_kingbuy/src/resource/model/model.dart';
import 'package:hues_kingbuy/src/resource/repository/repository.dart';
import 'package:hues_kingbuy/src/screen/base/base.dart';
import 'package:rxdart/rxdart.dart';

class SearchViewmodel extends BaseViewmodel {
  final ProductRepository productRepository;
  final _searchResult = BehaviorSubject<Product>();
  final _filterResult = BehaviorSubject<Product>();

  final _searchKeyword = BehaviorSubject<String>();
  final _categoryFilter = BehaviorSubject<int>();
  final _brandFilter = BehaviorSubject<int>();
  final _priceFilter = BehaviorSubject<Map>();

  SearchViewmodel({this.productRepository});

  String get searchKeyword => _searchKeyword.value;

  set searchKeyword(value) {
    _searchKeyword.add(value);
    notifyListeners();
  }

  Product get searchResult => _searchResult.value;

  set searchResult(value) {
    _searchResult.add(value);
    notifyListeners();
  }

  int get categoryFilter => _categoryFilter.value;

  set categoryFilter(value) {
    _categoryFilter.add(value);
    notifyListeners();
  }

  int get brandFilter => _brandFilter.value;

  set brandFilter(value) {
    _brandFilter.add(value);
    notifyListeners();
  }

  Map get priceFilter => _priceFilter.value;

  set priceFilter(value) {
    _priceFilter.add(value);
    notifyListeners();
  }

  Product get filterResult => _filterResult.value;

  set filterResult(value) {
    _filterResult.add(value);
    notifyListeners();
  }

  init() async {
    isLoading = true;
    priceFilter = {};
    isLoading = false;
  }

  onSearch(String keyword) async {
    if (keyword.length > 0) {
      searchKeyword = keyword;
      _searchKeyword
          .debounceTime(Duration(milliseconds: 1000))
          .listen((value) async {
        isLoading = true;
        searchResult =
            await productRepository.searchProduct(keyword: value, limit: 10);
        isLoading = false;
      });
    } else {
      searchResult = null;
    }
    notifyListeners();
  }

  Future onSubmitFilter() async {
    isLoading = true;
    filterResult = await productRepository.searchProduct(
      keyword: searchKeyword,
      category: categoryFilter,
      brand: brandFilter,
      // price: priceFilter,
    );
    isLoading = false;
  }

  List<ProductCategory> getCategoryFilter() {
    final categories = <ProductCategory>[];
    searchResult.data.forEach((element) {
      final item = ProductCategory(
        id: element.category.id,
        name: element.category.name,
        parent: element.category.parent,
      );
      if (!_isInList(categories, item)) {
        categories.add(item);
      }
    });
    return categories;
  }

  List<BrandItem> getBrandFilter() {
    final brands = <BrandItem>[];
    searchResult.data.forEach((element) {
      final item = BrandItem(
        id: element.brandId,
        name: element.brandName,
        content: element.brandInfo,
      );
      if (!_isInList(brands, item)) {
        brands.add(item);
      }
    });
    return brands;
  }

  _isInList(List list, var object) {
    return list.any((element) => element.id == object.id);
  }

  @override
  void dispose() {
    _searchKeyword.close();
    _searchResult.close();
    _filterResult.close();
    _brandFilter.close();
    _categoryFilter.close();
    _priceFilter.close();
    super.dispose();
  }
}
