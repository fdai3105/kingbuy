import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hues_kingbuy/src/resource/repository/repository.dart';
import 'package:hues_kingbuy/src/screen/base/base.dart';
import 'package:hues_kingbuy/src/screen/search/search.dart';
import 'package:hues_kingbuy/src/screen/widget/widget.dart';
import 'package:hues_kingbuy/src/utils/utils.dart';

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  GlobalKey<ScaffoldState> _drawerKey;
  bool showSearch;

  @override
  void initState() {
    _drawerKey = GlobalKey();
    showSearch = true;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseScreen<SearchViewmodel>(
      viewModel: SearchViewmodel(productRepository: ProductRepository()),
      onViewModelReady: (vm) async {
        vm.isLoading = true;
        await vm.init();
        vm.isLoading = false;
      },
      builder: (context, vm, child) {
        return Scaffold(
          resizeToAvoidBottomInset: false,
          key: _drawerKey,
          body: SafeArea(
            top: false,
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.only(
                      top: MediaQuery.of(context).padding.top + 10,
                      right: 10,
                      left: 10,
                      bottom: 10),
                  color: Colors.blueAccent,
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Flexible(
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: TextField(
                            onTap: () {
                              setState(() {
                                showSearch = true;
                              });
                            },
                            onSubmitted: (value) {
                              setState(() {
                                showSearch = false;
                              });
                            },
                            onChanged: (value) {
                              vm.onSearch(value);
                            },
                            textAlignVertical: TextAlignVertical.center,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              focusedBorder: InputBorder.none,
                              contentPadding:
                                  EdgeInsets.symmetric(horizontal: 10),
                              prefixIcon: Icon(Icons.search),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(width: 10),
                      showSearch
                          ? GestureDetector(
                              onTap: () => Navigator.pop(context),
                              behavior: HitTestBehavior.opaque,
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                  horizontal: 10,
                                  vertical: 10,
                                ),
                                child: Center(
                                  child: Text(
                                    "Huỷ",
                                    style: TextStyle(
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ),
                            )
                          : IconButton(
                              icon: Icon(Icons.filter_alt_outlined,
                                  color: Colors.white),
                              onPressed: () {
                                _drawerKey.currentState.openEndDrawer();
                              },
                            ),
                    ],
                  ),
                ),
                Expanded(child: _body(vm)),
              ],
            ),
          ),
          endDrawer: FilterEndDrawerWidget(vm: vm),
        );
      },
    );
  }

  Widget _body(SearchViewmodel vm) {
    if (vm.isLoading) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }
    if (vm.searchResult == null) {
      return Center(
        child: Text("Tìm kiếm nào"),
      );
    } else {
      if (vm.searchResult.data.length == 0) {
        return Center(
          child: Text("Không tìm thấy sản phẩm"),
        );
      }
      final _items = vm.searchResult.data;
      return Stack(
        children: [
          Visibility(
            visible: showSearch,
            child: ListView.builder(
              itemCount: _items.length,
              itemBuilder: (context, index) {
                return ListTile(
                  title: Text(_items[index].name),
                );
              },
            ),
          ),
          Visibility(
            visible: !showSearch,
            child: GridView.builder(
              itemCount: _items.length,
              shrinkWrap: true,
              padding: EdgeInsets.all(6),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                crossAxisSpacing: 6,
                mainAxisSpacing: 6,
                childAspectRatio: 0.74,
              ),
              itemBuilder: (context, index) {
                return ItemProduct(
                  onTap: () {
                    Navigator.pushNamed(
                      context,
                      Routes.productScreen,
                      arguments: {"product": _items[index]},
                    );
                  },
                  item: _items[index],
                  margin: EdgeInsets.zero,
                );
              },
            ),
          ),
        ],
      );
    }
  }
}

class FilterEndDrawerWidget extends StatelessWidget {
  const FilterEndDrawerWidget({
    Key key,
    @required this.vm,
  }) : super(key: key);

  final SearchViewmodel vm;

  @override
  Widget build(BuildContext context) {
    if (vm.isLoading) {
      return Drawer(child: Center(child: CircularProgressIndicator()));
    } else {
      return Drawer(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              color: Colors.blueAccent,
              padding: EdgeInsets.symmetric(vertical: 20),
              child: Column(
                children: [
                  SizedBox(height: MediaQuery.of(context).padding.top),
                  Row(
                    children: [
                      Icon(
                        Icons.filter_alt_outlined,
                        color: Colors.white,
                      ),
                      Text(
                        "Bộ lọc tìm kiếm",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(height: 20),
            Text("Danh mục"),
            FilterButtonWidget(
              children: vm
                  .getCategoryFilter()
                  .map(
                    (e) => FilterButtonItem(
                      text: e.name,
                      value: e.id,
                    ),
                  )
                  .toList(),
              onSelected: (value) {
                vm.categoryFilter = value;
              },
              initialValue: vm.categoryFilter,
            ),
            SizedBox(height: 20),
            Text("Thương hiệu"),
            FilterButtonWidget(
              children: vm
                  .getBrandFilter()
                  .map(
                    (e) => FilterButtonItem(
                      text: e.name,
                      value: e.id,
                    ),
                  )
                  .toList(),
              onSelected: (value) {
                vm.brandFilter = value;
              },
              initialValue: vm.brandFilter,
            ),
            SizedBox(height: 20),
            Text("Giá"),
            FilterButtonWidget(
              children: [
                FilterButtonItem(
                  text: "8-10 triệu",
                  value: "price[from]=8000000&price[to]=10000000",
                ),
                FilterButtonItem(
                  text: "10 - 15 triệu",
                  value: "price[from]=10000000&price[to]=15000000",
                ),
                FilterButtonItem(
                  text: "15 - 20 triệu",
                  value: "price[from]=15000000&price[to]=20000000",
                ),
                FilterButtonItem(
                  text: "20 - 25 triệu",
                  // value: "price[from]=20000000&price[to]=25000000",
                  value: {"from": 20000000, "to": 25000000},
                ),
              ],
              onSelected: (value) {
                vm.priceFilter = value;
              },
            ),
            Row(
              children: [
                FlatButton(
                  onPressed: () {},
                  child: Text("Thiết lập lại"),
                ),
                FlatButton(
                  onPressed: () {
                    vm.onSubmitFilter();
                    Navigator.pop(context);
                  },
                  child: Text("Áp dụng"),
                ),
              ],
            )
          ],
        ),
      );
    }
  }
}
