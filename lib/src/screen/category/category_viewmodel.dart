import 'package:hues_kingbuy/src/resource/model/model.dart';
import 'package:hues_kingbuy/src/resource/repository/repository.dart';
import 'package:hues_kingbuy/src/screen/base/base.dart';
import 'package:rxdart/rxdart.dart';

class CategoryViewmodel extends BaseViewmodel {
  final ProductRepository productRepository;

  final _products = BehaviorSubject<Product>();
  final _productLoading = BehaviorSubject<bool>();

  final _filterCategory = BehaviorSubject<List<ProductCategory>>();
  final _filterBrand = BehaviorSubject<List<ProductBrand>>();

  final _selectCategory = BehaviorSubject<ProductCategory>();
  final _selectBrand = BehaviorSubject<ProductBrand>();

  CategoryViewmodel(this.productRepository);

  Product get products => _products.value;

  set products(value) {
    _products.add(value);
    notifyListeners();
  }

  bool get productLoading => _productLoading.value;

  set productLoading(value) {
    _productLoading.add(value);
    notifyListeners();
  }

  List<ProductCategory> get filterCategory => _filterCategory.value;

  set filterCategory(List<ProductCategory> value) {
    _filterCategory.add(value);
    notifyListeners();
  }

  List<ProductBrand> get filterBrand => _filterBrand.value;

  set filterBrand(value) {
    _filterBrand.add(value);
    notifyListeners();
  }

  ProductCategory get selectCategory => _selectCategory.value;

  set selectCategory(value) {
    _selectCategory.add(value);
    notifyListeners();
  }

  ProductBrand get selectBrand => _selectBrand.value;

  set selectBrand(value) {
    _selectBrand.add(value);
    notifyListeners();
  }

  Future<void> init(int id) async {
    isLoading = true;
    productLoading = true;
    await getProducts(id);
    getFilterCategory();
    getFilterBrand();
    productLoading = false;
    isLoading = false;
  }

  Future getProducts(int categoryID) async {
    products = await productRepository.getProductsByCategory(
      categoryID: categoryID,
    );
  }

  void getFilterCategory() {
    final list = <ProductCategory>[];
    for (final item in products.data) {
      if (list.where((element) => element.id == item.brandId).length == 0) {
        list.add(ProductCategory(id: item.brandId, name: item.brandName));
      }
    }
    filterCategory = list;
  }

  void getFilterBrand() {
    final list = <ProductBrand>[];
    for (final item in products.data) {
      if (list.where((element) => element.id == item.category.id).length == 0) {
        list.add(ProductBrand(id: item.category.id, name: item.category.name));
      }
    }
    filterBrand = list;
  }

  @override
  void dispose() async {
    await _products.drain();
    _products.close();
    await _productLoading.drain();
    _productLoading.close();
    await _filterBrand.drain();
    _filterBrand.close();
    await _filterCategory.drain();
    _filterCategory.close();
    super.dispose();
  }
}
