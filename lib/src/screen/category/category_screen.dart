import 'package:flutter/material.dart';
import 'package:hues_kingbuy/src/resource/model/model.dart';
import 'package:hues_kingbuy/src/screen/category/category.dart';
import 'package:hues_kingbuy/src/screen/widget/widget.dart';
import 'package:hues_kingbuy/src/utils/utils.dart';
import 'package:provider/provider.dart';

class CategoryScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Map arguments = ModalRoute.of(context).settings.arguments;
    final CategoryItem item = arguments["category"];
    Provider.of<CategoryViewmodel>(context, listen: false).init(item.id);

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red.shade700,
      ),
      body: Consumer<CategoryViewmodel>(
        builder: (context, vm, child) {
          if (vm.isLoading) {
            return Center(child: CircularProgressIndicator());
          } else {
            if (vm.products.data.length == 0) {
              return Center(child: Text("Không có dữ liệu"));
            } else {
              return SingleChildScrollView(
                padding: EdgeInsets.symmetric(horizontal: 8),
                child: Column(
                  children: [
                    item.parent == null
                        ? Row(
                            children: [
                              Image.asset("assets/icons/homeOff.png"),
                              Text(item.name),
                            ],
                          )
                        : Padding(
                            padding: const EdgeInsets.symmetric(vertical: 20),
                            child: Row(
                              children: [
                                Image.asset(
                                  "assets/icons/homeOff.png",
                                  height: 20,
                                  width: 20,
                                  color: Colors.black87,
                                ),
                                SizedBox(width: 6),
                                Image.asset(
                                  "assets/icons/e-path.png",
                                  height: 10,
                                  width: 10,
                                  color: Colors.black87,
                                ),
                                SizedBox(width: 6),
                                Text(
                                  item.parent.name,
                                  style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                                SizedBox(width: 6),
                                Image.asset(
                                  "assets/icons/e-path.png",
                                  height: 10,
                                  width: 10,
                                  color: Colors.black87,
                                ),
                                SizedBox(width: 6),
                                Text(
                                  item.name,
                                  style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ],
                            ),
                          ),
                    SizedBox(height: 10),
                    vm.productLoading
                        ? Center(child: CircularProgressIndicator())
                        : GridView.builder(
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            itemCount: vm.products.data.length,
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 2,
                              crossAxisSpacing: 6,
                              mainAxisSpacing: 6,
                              childAspectRatio: 0.72,
                            ),
                            itemBuilder: (context, index) {
                              return ItemProduct(
                                onTap: () {
                                  Navigator.pushNamed(
                                    context,
                                    Routes.productScreen,
                                    arguments: {
                                      "product": vm.products.data[index]
                                    },
                                  );
                                },
                                item: vm.products.data[index],
                                margin: EdgeInsets.zero,
                              );
                            },
                          )
                  ],
                ),
              );
            }
          }
        },
      ),
      endDrawer: Drawer(
        child: Column(
          children: [
            Consumer<CategoryViewmodel>(
              builder: (context, vm, child) {
                if (vm.isLoading) {
                  return Center(child: CircularProgressIndicator());
                } else {
                  return Column(
                    children: [
                      SizedBox(height: 10),
                      Text('Danh mục'),
                      ListView.builder(
                        shrinkWrap: true,
                        itemCount: vm.filterCategory.length,
                        itemBuilder: (context,index) {
                          return Text(vm.filterCategory[index].name);
                        },
                      ),
                      SizedBox(height: 10),
                      Text('Thương hiệu'),
                      ListView.builder(
                        shrinkWrap: true,
                        itemCount: vm.filterBrand.length,
                        itemBuilder: (context,index) {
                          return Text(vm.filterBrand[index].name);
                        },
                      ),
                    ],
                  );
                }
              },
            )
          ],
        ),
      ),
    );
  }
}
