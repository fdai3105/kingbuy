import 'package:hues_kingbuy/src/resource/model/Ward.dart';

import 'package:hues_kingbuy/src/resource/model/city.dart';
import 'package:hues_kingbuy/src/resource/model/district.dart';
import 'package:hues_kingbuy/src/resource/model/user_address.dart';
import 'package:hues_kingbuy/src/resource/repository/address_repository.dart';
import 'package:hues_kingbuy/src/screen/base/base_viewmodel.dart';
import 'package:rxdart/rxdart.dart';

class AddressViewmodel extends BaseViewmodel {
  final AddressRepository addressRepository;

  AddressViewmodel(this.addressRepository);

  final _userAddress = BehaviorSubject<List<UserAddress>>();
  final _cities = BehaviorSubject<City>();
  final _districts = BehaviorSubject<District>();
  final _wards = BehaviorSubject<Ward>();

  final _currentCity = BehaviorSubject<CityItem>();
  final _currentDistrict = BehaviorSubject<DistrictItem>();
  final _currentWard = BehaviorSubject<WardItem>();

  List<UserAddress> get userAddress => _userAddress.value;

  set userAddress(value) {
    _userAddress.add(value);
    notifyListeners();
  }

  City get cities => _cities.value;

  set cities(value) {
    _cities.add(value);
    notifyListeners();
  }

  District get districts => _districts.value;

  set districts(value) {
    _districts.add(value);
    notifyListeners();
  }

  Ward get wards => _wards.value;

  set wards(value) {
    _wards.add(value);
    notifyListeners();
  }

  CityItem get currentCity => _currentCity.value;

  set currentCity(value) {
    _currentCity.add(value);
    notifyListeners();
  }

  DistrictItem get currentDistrict => _currentDistrict.value;

  set currentDistrict(value) {
    _currentDistrict.add(value);
    notifyListeners();
  }

  WardItem get currentWard => _currentWard.value;

  set currentWard(value) {
    _currentWard.add(value);
    notifyListeners();
  }

  init() async {
    isLoading = true;
    userAddress = <UserAddress>[];
    cities = await addressRepository.getCities();
    isLoading = false;
  }

  void addAddress(UserAddress address) {
    userAddress.add(address);
    notifyListeners();
  }

  void setDistricts(String id) async {
    districts = null;
    wards = null;
    currentDistrict = null;
    currentWard = null;

    final districtsRepo = await addressRepository.getDistrict(id);
    districts = districtsRepo;
  }

  void setWard(String id) async {
    final wardsRepo = await addressRepository.getWards(id);
    wards = wardsRepo;
  }

  @override
  void dispose() async {
    await _userAddress.drain();
    _userAddress.close();
    await _cities.drain();
    _cities.close();
    super.dispose();
  }
}
