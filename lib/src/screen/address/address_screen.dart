import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hues_kingbuy/src/screen/address/address_viewmodel.dart';
import 'package:hues_kingbuy/src/screen/cart/cart.dart';
import 'package:hues_kingbuy/src/utils/route.dart';
import 'package:provider/provider.dart';

class AddressScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final cartVM = Provider.of<CartViewmodel>(context, listen: true);

    return Scaffold(
      appBar: AppBar(
        title: Text('Địa chỉ giao hàng'),
        actions: [
          IconButton(
            icon: Icon(Icons.add),
            onPressed: () =>
                Navigator.pushNamed(context, Routes.addressAddScreen),
          ),
        ],
      ),
      body: SafeArea(
        child: Consumer<AddressViewmodel>(
          builder: (context, vm, child) {
            if (vm.isLoading) {
              return Center(child: CircularProgressIndicator());
            } else {
              return ListView.builder(
                itemCount: vm.userAddress.length,
                itemBuilder: (context, index) {
                  final address = vm.userAddress[index];
                  return ListTile(
                    onTap: () {
                      cartVM.address = address;
                      Future.delayed(Duration(milliseconds: 400), () {
                        Navigator.pop(context);
                      });
                    },
                    leading: Radio(
                      value: true,
                      groupValue: cartVM.address == address,
                      onChanged: (value) {
                        cartVM.address = address;
                        Future.delayed(Duration(milliseconds: 400), () {
                          Navigator.pop(context);
                        });
                      },
                      activeColor: Colors.red.shade700,
                    ),
                    title: Text(address.fullName ?? ''),
                    subtitle: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "${address.address}, ${address.ward}, ${address.district}, ${address.city}",
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                        ),
                        Text(
                          "${address.phoneNumber} "
                          "${address.phoneNumberSecond == '' ? '' : ' - ' + address.phoneNumberSecond}",
                        ),
                      ],
                    ),
                  );
                },
              );
            }
          },
        ),
      ),
    );
  }
}
