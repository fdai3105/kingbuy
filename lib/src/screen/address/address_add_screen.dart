import 'package:flutter/material.dart';
import 'package:hues_kingbuy/src/resource/model/Ward.dart';
import 'package:hues_kingbuy/src/resource/model/city.dart';
import 'package:hues_kingbuy/src/resource/model/district.dart';
import 'package:hues_kingbuy/src/resource/model/user_address.dart';
import 'package:hues_kingbuy/src/screen/address/address_viewmodel.dart';
import 'package:provider/provider.dart';

class AddressAddScreen extends StatelessWidget {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final UserAddress address = UserAddress();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text('Thêm địa chỉ mới'),
        actions: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: FlatButton(
              onPressed: () => _save(context),
              child: Text('Lưu'),
              minWidth: double.minPositive,
              color: Colors.grey,
              textColor: Colors.white,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
            ),
          ),
        ],
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                InputField(
                  label: 'Họ và tên',
                  hint: 'Điền họ và tên',
                  onSaved: (value) {
                    address.fullName = value;
                  },
                ),
                InputField(
                  label: 'Số điện thoại',
                  hint: 'Điền số điện thoại chính',
                  onSaved: (value) {
                    address.phoneNumber = value;
                  },
                ),
                InputField(
                  label: 'Số điện thoại phụ',
                  hint: 'Điền số điện thoại phụ nếu có',
                  validation: false,
                  onSaved: (value) {
                    address.phoneNumberSecond = value;
                  },
                ),
                AddressSelect(
                  onCitySelected: (city) {
                    address.city = city.name;
                  },
                  onDistrictSelected: (district) {
                    address.district = district.nameWithType;
                  },
                  onWardSelected: (ward) {
                    address.ward = ward.nameWithType;
                  },
                ),
                InputField(
                  label: 'Địa chỉ cụ thể',
                  hint: 'Nhập địa chỉ cụ thể số nhà, tên toà nhà,...',
                  onSaved: (value) {
                    address.address = value;
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _save(BuildContext context) {
    if (_formKey.currentState.validate()) {
      if (address.city == null ||
          address.district == null ||
          address.ward == null) {
        print(address.city);
        print(address.address);
        print(address.ward);
        return;
      }
      _formKey.currentState.save();
      Provider.of<AddressViewmodel>(context, listen: false).addAddress(address);
      Navigator.pop(context);
    }
  }
}

class InputField extends StatelessWidget {
  const InputField({
    Key key,
    this.label,
    @required this.onSaved,
    @required this.hint,
    this.validation = true,
  }) : super(key: key);

  final String label;
  final String hint;
  final Function(String) onSaved;
  final bool validation;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 8),
      padding: EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(label ?? ''),
          TextFormField(
            decoration: InputDecoration(
              hintText: hint ?? '',
              hintStyle: TextStyle(color: Colors.grey),
            ),
            onSaved: onSaved,
            validator: (value) {
              if (validation) {
                if (value.isEmpty) {
                  return label + ' không được để trống';
                } else {
                  return null;
                }
              } else {
                return null;
              }
            },
          ),
        ],
      ),
    );
  }
}

class AddressSelect extends StatelessWidget {
  final Function(CityItem) onCitySelected;
  final Function(DistrictItem) onDistrictSelected;
  final Function(WardItem) onWardSelected;

  const AddressSelect(
      {Key key,
      @required this.onCitySelected,
      @required this.onDistrictSelected,
      @required this.onWardSelected})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final vm = Provider.of<AddressViewmodel>(context, listen: true);
    return Column(
      children: [
        SizedBox(height: 10),
        Row(
          mainAxisSize: MainAxisSize.max,
          children: [
            Flexible(
              flex: 1,
              child: DropdownButton<CityItem>(
                items: vm.cities.data.map((e) {
                  return DropdownMenuItem<CityItem>(
                    value: e,
                    child: Text(e.name),
                  );
                }).toList(),
                onChanged: (value) {
                  vm.currentCity = value;
                  vm.setDistricts(value.code);
                  onCitySelected(value);
                },
                hint: Text('Chọn tỉnh/thành phố'),
                value: vm.currentCity,
                isExpanded: true,
              ),
            ),
            SizedBox(width: 20),
            Flexible(
              flex: 1,
              child: DropdownButton<DistrictItem>(
                items: vm.districts == null
                    ? []
                    : vm.districts.data.map((e) {
                        return DropdownMenuItem(
                          value: e,
                          child: Text(e.nameWithType),
                        );
                      }).toList(),
                onChanged: (value) {
                  vm.currentDistrict = value;
                  vm.setWard(value.code);
                  onDistrictSelected(value);
                },
                value: vm.currentDistrict,
                hint: Text('Chọn quận/huyện'),
                isExpanded: true,
              ),
            ),
          ],
        ),
        SizedBox(height: 10),
        DropdownButton<WardItem>(
          items: vm.wards == null
              ? []
              : vm.wards.data.map((e) {
                  return DropdownMenuItem(
                    value: e,
                    child: Text(e.nameWithType),
                  );
                }).toList(),
          onChanged: (value) {
            vm.currentWard = value;
            onWardSelected(value);
          },
          value: vm.currentWard,
          hint: Text('Chọn phường/xã'),
          isExpanded: true,
        ),
        SizedBox(height: 10),
      ],
    );
  }
}
