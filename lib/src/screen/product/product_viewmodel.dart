import 'package:hues_kingbuy/src/resource/model/model.dart';
import 'package:hues_kingbuy/src/resource/model/purchased_together.dart';
import 'package:hues_kingbuy/src/resource/repository/repository.dart';
import 'package:hues_kingbuy/src/screen/base/base.dart';
import 'package:rxdart/rxdart.dart';

class ProductViewmodel extends BaseViewmodel {
  final ProductRepository productRepository;
  final AddressRepository addressRepository;

  ProductViewmodel({this.productRepository, this.addressRepository});

  final _rating = BehaviorSubject<Rating>();
  final _address = BehaviorSubject<Address>();
  final _review = BehaviorSubject<Review>();
  final _related = BehaviorSubject<Product>();
  final _purchasedTogether = BehaviorSubject<PurchasedTogether>();

  Rating get rating => _rating.value;

  set rating(value) {
    _rating.add(value);
    notifyListeners();
  }

  Address get address => _address.value;

  set address(value) {
    _address.add(value);
    notifyListeners();
  }

  Review get review => _review.value;

  set review(value) {
    _review.add(value);
    notifyListeners();
  }

  Product get related => _related.value;

  set related(value) {
    _related.add(value);
    notifyListeners();
  }

  PurchasedTogether get purchasedTogether => _purchasedTogether.value;

  set purchasedTogether(value) {
    _purchasedTogether.add(value);
    notifyListeners();
  }

  init(int productID) async {
    isLoading = true;
    rating = await productRepository.getRating(productID);
    address = await addressRepository.getAddressStore();
    review = await productRepository.getReview(productID);
    related = await productRepository.getRelated(productID);
    purchasedTogether = await productRepository.getPurchasedTogether(productID);
    isLoading = false;
  }

  @override
  void dispose() async {
    await _rating.drain();
    _rating.close();
    await _address.drain();
    _address.close();
    await _related.drain();
    _related.close();
    await _purchasedTogether.drain();
    _purchasedTogether.close();
    super.dispose();
  }
}
