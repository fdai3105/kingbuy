import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:hues_kingbuy/src/config/config.dart';
import 'package:hues_kingbuy/src/resource/model/model.dart';
import 'package:hues_kingbuy/src/screen/base/base.dart';
import 'package:hues_kingbuy/src/screen/cart/cart.dart';
import 'package:hues_kingbuy/src/screen/widget/widget.dart';
import 'package:hues_kingbuy/src/utils/utils.dart';
import 'package:intl/intl.dart';
import 'package:hues_kingbuy/src/resource/repository/repository.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:provider/provider.dart';
import 'product_viewmodel.dart';

class ProductScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Map arguments = ModalRoute.of(context).settings.arguments;
    final ProductItem item = arguments["product"];
    final cartVm = Provider.of<CartViewmodel>(context);

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red.shade700,
        actions: [
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () {
              Navigator.pushNamed(context, Routes.searchScreen);
            },
          ),
          IconButton(
            icon: Icon(Icons.home),
            onPressed: () {
              Navigator.pushNamed(context, Routes.dashboardScreen);
            },
          ),
          Stack(
            children: [
              Align(
                alignment: Alignment.center,
                child: IconButton(
                  icon: Icon(Icons.shopping_cart),
                  onPressed: () {
                    Navigator.pushNamed(context, Routes.cartScreen);
                  },
                ),
              ),
              cartVm.listCart.length == 0
                  ? Container()
                  : Positioned(
                      top: 2,
                      right: 2,
                      child: Container(
                        padding: EdgeInsets.all(6),
                        decoration: BoxDecoration(
                          color: Colors.blue,
                          shape: BoxShape.circle,
                        ),
                        child: Text(cartVm.listCart.length.toString()),
                      ),
                    )
            ],
          ),
        ],
      ),
      body: SafeArea(
        child: BaseScreen<ProductViewmodel>(
          viewModel: ProductViewmodel(
            productRepository: ProductRepository(),
            addressRepository: AddressRepository(),
          ),
          onViewModelReady: (vm) async {
            await vm.init(item.id);
          },
          builder: (context, vm, child) {
            return SingleChildScrollView(
              child: Container(
                color: Colors.grey.shade300,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      color: Colors.white,
                      child: ImageSlideWidget(item: item.imageSourceList),
                    ),
                    Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: 10, vertical: 8),
                      color: Colors.white,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            item.name,
                            style: TextStyle(
                              color: Colors.black87,
                              fontSize: 20,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          SizedBox(height: 8),
                          RatingBar(
                            ratingWidget: RatingWidget(
                              empty: Icon(
                                Icons.star_outlined,
                                color: Colors.grey,
                              ),
                              full: Icon(
                                Icons.star_outlined,
                                color: Colors.amber,
                              ),
                              half: Icon(
                                Icons.star_half_outlined,
                                color: Colors.amber,
                              ),
                            ),
                            itemCount: 5,
                            initialRating: item.star.toDouble(),
                            itemSize: 24,
                            ignoreGestures: true,
                            onRatingUpdate: (double value) {},
                          ),
                          SizedBox(height: 8),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Row(
                                children: [
                                  Text(
                                    CustomFormat.formatMoney(item.price),
                                    style: TextStyle(
                                      color: Colors.red.shade700,
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  SizedBox(width: 10),
                                  Text(
                                    CustomFormat.formatMoney(item.salePrice),
                                    style: TextStyle(
                                        decoration: TextDecoration.lineThrough),
                                  ),
                                ],
                              ),
                              Container(
                                padding: EdgeInsets.all(8),
                                decoration: BoxDecoration(
                                  color: Colors.red.shade700,
                                  borderRadius: BorderRadius.circular(20),
                                ),
                                child: Text(
                                  "-${item.saleOff}%",
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                              )
                            ],
                          ),
                          Row(
                            children: [
                              Text("Tình trạng: "),
                              Text(item.status == 0 ? "Hết hàng" : "Còn hàng"),
                            ],
                          ),
                          Center(
                            child: FlatButton(
                              onPressed: () {},
                              child: Text("Đặt mua trả góp"),
                              color: Colors.deepOrange,
                              textColor: Colors.white,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 10),
                    item.gifts.length == 0
                        ? Container()
                        : ListGift(gifts: item.gifts),
                    item.gifts.length == 0 ? SizedBox() : SizedBox(height: 10),
                    Container(
                      color: Colors.white,
                      padding:
                          EdgeInsets.symmetric(horizontal: 10, vertical: 8),
                      child: ListAddress(
                          address: vm.address, isLoading: vm.isLoading),
                    ),
                    SizedBox(height: 10),
                    Container(
                      padding: EdgeInsets.symmetric(
                        horizontal: 10,
                        vertical: 8,
                      ),
                      color: Colors.white,
                      child: CustomExpansion(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Thông tin sản phẩm",
                              style: TextStyle(
                                color: Colors.black87,
                                fontSize: 18,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                            item.content == null
                                ? Padding(
                                    padding: const EdgeInsets.all(18),
                                    child: Center(
                                        child: Text("Không có thông tin")))
                                : Html(data: item.content),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: 10),
                    Container(
                      padding:
                          EdgeInsets.symmetric(vertical: 8, horizontal: 10),
                      color: Colors.white,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Thông số kỹ thuật",
                            style: TextStyle(
                              color: Colors.black87,
                              fontSize: 18,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          item.specifications == null
                              ? Padding(
                                  padding: const EdgeInsets.all(18),
                                  child:
                                      Center(child: Text("Không có thông tin")))
                              : Html(data: item.specifications),
                        ],
                      ),
                    ),
                    SizedBox(height: 10),
                    Container(
                      color: Colors.white,
                      padding: EdgeInsets.symmetric(
                        vertical: 10,
                        horizontal: 8,
                      ),
                      child: vm.isLoading
                          ? Center(child: CircularProgressIndicator())
                          : Column(
                              children: [
                                VoteProduct(
                                    rating: vm.rating, isLoading: vm.isLoading),
                                SizedBox(height: 6),
                                Container(
                                  margin: EdgeInsets.symmetric(horizontal: 20),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    border: Border.all(
                                      color: Colors.red.shade700,
                                    ),
                                  ),
                                  child: FlatButton(
                                    onPressed: () {},
                                    minWidth: double.infinity,
                                    splashColor: Colors.transparent,
                                    highlightColor: Colors.transparent,
                                    child: Text("Viết đánh giá"),
                                  ),
                                ),
                                SizedBox(height: 6),
                                ReviewProduct(
                                  review: vm.review,
                                  isLoading: vm.isLoading,
                                ),
                              ],
                            ),
                    ),
                    SizedBox(height: 10),
                    Container(
                      color: Colors.white,
                      padding: EdgeInsets.symmetric(
                        vertical: 10,
                        horizontal: 8,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Hỏi đáp về sản phẩm",
                            style: TextStyle(
                              color: Colors.black87,
                              fontSize: 18,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          SizedBox(height: 10),
                          Center(
                            child: Padding(
                              padding: const EdgeInsets.all(18),
                              child: Center(
                                child: Text(
                                  "Không có câu hỏi nào",
                                  style: TextStyle(
                                    color: Colors.red.shade700,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(height: 10),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 40),
                            child: FlatButton(
                              onPressed: () {},
                              minWidth: double.infinity,
                              color: Colors.red.shade700,
                              textColor: Colors.white,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              child: Text("Đặt câu hỏi cho sản phẩm"),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 10),
                    Container(
                      color: Colors.white,
                      padding: EdgeInsets.symmetric(
                        vertical: 10,
                        horizontal: 8,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Thường được mua cùng",
                            style: TextStyle(
                              color: Colors.black87,
                              fontSize: 18,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          SizedBox(height: 10),
                          PurchasedTogetherWidget(
                            product: vm.purchasedTogether,
                            isLoading: vm.isLoading,
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 10),
                    Container(
                      color: Colors.white,
                      width: double.infinity,
                      padding: EdgeInsets.symmetric(vertical: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 8),
                            child: Text(
                              "Sản phẩm liên quan",
                              style: TextStyle(
                                color: Colors.black87,
                                fontSize: 18,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                          SizedBox(height: 10),
                          RelatedProduct(
                            product: vm.related,
                            isLoading: vm.isLoading,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
      bottomNavigationBar: Container(
        height: 60,
        decoration: BoxDecoration(color: Colors.white, boxShadow: [
          BoxShadow(
            color: Colors.black26,
            blurRadius: 10,
            offset: Offset(0, 0.75),
          )
        ]),
        child: Row(
          children: [
            ItemBottomNavigation(
              onTap: () {},
              label: "Chat zalo",
              icon: "assets/icons/zalo-chat-logo-png-300.png",
            ),
            ItemBottomNavigation(
              onTap: () {},
              label: "Chat facebook",
              icon: "assets/icons/facebook-messenger-13164.png",
            ),
            ItemBottomNavigation(
              onTap: () {
                _addToCartDialog(context, item);
                cartVm.addProduct(item);
              },
              label: "Thêm vào giỏ",
              icon: "assets/icons/shopping-cart-black.png",
            ),
            Flexible(
              child: FlatButton(
                onPressed: () {},
                color: Colors.red.shade700,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(0)),
                textColor: Colors.white,
                height: double.infinity,
                minWidth: double.infinity,
                child: Text("Mua ngay"),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _addToCartDialog(BuildContext context, ProductItem item) {
    showModalBottomSheet(
        context: (context),
        builder: (context) {
          return Container(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("sản phẩm đã thêm vào giỏ hàng"),
                    IconButton(
                        icon: Icon(Icons.close),
                        onPressed: () => Navigator.pop(context)),
                  ],
                ),
                ListTile(
                  leading: CachedNetworkImage(
                    imageUrl: AppEndpoint.BASE + item.imageSource,
                  ),
                  title: Text(item.name),
                  subtitle: Text(CustomFormat.formatMoney(item.price)),
                ),
                FlatButton(
                  onPressed: () =>
                      Navigator.pushNamed(context, Routes.cartScreen),
                  child: Text("Xem giỏ hàng"),
                  minWidth: double.infinity,
                  color: Colors.red.shade700,
                  textColor: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)),
                ),
              ],
            ),
          );
        });
  }
}

class ItemBottomNavigation extends StatelessWidget {
  final Function onTap;
  final String label;
  final String icon;

  const ItemBottomNavigation({
    Key key,
    this.onTap,
    this.label,
    this.icon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        decoration: BoxDecoration(
          border: Border(
            right: BorderSide(
              color: Colors.black26,
            ),
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset(
                icon ?? "",
                width: 34,
                height: 34,
              ),
              Text(label ?? ""),
            ],
          ),
        ),
      ),
    );
  }
}

class PurchasedTogetherWidget extends StatelessWidget {
  final PurchasedTogether product;
  final bool isLoading;

  const PurchasedTogetherWidget({
    Key key,
    @required this.product,
    @required this.isLoading,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (isLoading) {
      return Center(child: CircularProgressIndicator());
    } else {
      if (product.data.products.length == 0) {
        return Padding(
          padding: const EdgeInsets.all(18),
          child: Center(
            child: Text("Không có dữ liệu"),
          ),
        );
      } else {
        return Column(
          children: [
            Container(
              height: 100,
              width: double.infinity,
              child: Center(
                child: ListView.separated(
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemCount: product.data.products.length,
                  separatorBuilder: (context, index) {
                    if (index % 2 == 0) {
                      return Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: Icon(Icons.add),
                      );
                    } else {
                      return Container();
                    }
                  },
                  itemBuilder: (context, index) {
                    return CachedNetworkImage(
                      imageUrl: AppEndpoint.BASE +
                          product.data.products[index].imageSource,
                      width: 100,
                      fit: BoxFit.cover,
                    );
                  },
                ),
              ),
            ),
            SizedBox(height: 6),
            ListPurchasedTogether(buyTogetherProduct: product.data.products)
          ],
        );
      }
    }
  }
}

class ListPurchasedTogether extends StatefulWidget {
  final List<PurchasedTogetherProduct> buyTogetherProduct;

  const ListPurchasedTogether({Key key, @required this.buyTogetherProduct})
      : super(key: key);

  @override
  _ListPurchasedTogetherState createState() => _ListPurchasedTogetherState();
}

class _ListPurchasedTogetherState extends State<ListPurchasedTogether> {
  List<bool> _selectedList;

  @override
  void initState() {
    _selectedList =
        List.generate(widget.buyTogetherProduct.length, (index) => false);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final _count = _selectedList.where((element) => element).toList().length;
    return Column(
      children: [
        ListView.builder(
          shrinkWrap: true,
          itemCount: widget.buyTogetherProduct.length,
          itemBuilder: (context, index) {
            return Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Checkbox(
                  checkColor: Colors.white,
                  activeColor: Colors.red.shade700,
                  value: _selectedList[index],
                  onChanged: (value) {
                    setState(() {
                      _selectedList[index] = !_selectedList[index];
                    });
                  },
                ),
                Flexible(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        widget.buyTogetherProduct[index].name,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                      Text(
                        CustomFormat.formatMoney(
                            widget.buyTogetherProduct[index].price),
                        style: TextStyle(
                          color: Colors.black87,
                          fontSize: 15,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            );
          },
        ),
        SizedBox(height: 6),
        FlatButton(
          onPressed: _count == 0 ? null : () {},
          color: Colors.red.shade700,
          disabledColor: Colors.red.shade200,
          disabledTextColor: Colors.white.withOpacity(0.8),
          textColor: Colors.white,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
          child: Text(
            "Thêm $_count sản phẩm vào giỏ hàng",
          ),
        )
      ],
    );
  }
}

class RelatedProduct extends StatelessWidget {
  final Product product;
  final bool isLoading;

  const RelatedProduct({
    Key key,
    @required this.product,
    @required this.isLoading,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (isLoading) {
      return Center(child: CircularProgressIndicator());
    } else {
      if (product.data.length == 0) {
        return Padding(
          padding: const EdgeInsets.all(18),
          child: Center(child: Text("Không có dữ liệu")),
        );
      } else {
        return SizedBox(
          height: 270,
          child: ListView.builder(
            shrinkWrap: true,
            padding: EdgeInsets.symmetric(horizontal: 8),
            scrollDirection: Axis.horizontal,
            itemCount: product.data.length,
            itemBuilder: (context, index) {
              return ItemProduct(
                onTap: () {},
                item: product.data[index],
              );
            },
          ),
        );
      }
    }
  }
}

class ReviewProduct extends StatelessWidget {
  const ReviewProduct({
    Key key,
    @required this.review,
    @required this.isLoading,
  }) : super(key: key);

  final Review review;
  final bool isLoading;

  @override
  Widget build(BuildContext context) {
    if (review.data.rows.length == 0) {
      return Padding(
        padding: const EdgeInsets.all(18),
        child: Center(
          child: Text("Chưa có đánh giá nào"),
        ),
      );
    } else {
      return Column(
        children: [
          ListView.separated(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: review.data.rows.length,
            separatorBuilder: (context, index) {
              return Divider(color: Colors.grey);
            },
            itemBuilder: (context, index) {
              return ItemReview(item: review.data.rows[index]);
            },
          ),
          Divider(color: Colors.grey),
          FlatButton(
            onPressed: () {},
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  "Xem tất cả",
                  style: TextStyle(
                    color: Colors.blue,
                  ),
                ),
                Icon(
                  Icons.keyboard_arrow_right,
                  color: Colors.blue,
                ),
              ],
            ),
          ),
        ],
      );
    }
  }
}

class VoteProduct extends StatelessWidget {
  const VoteProduct({
    Key key,
    @required this.rating,
    @required this.isLoading,
  }) : super(key: key);

  final Rating rating;
  final bool isLoading;

  @override
  Widget build(BuildContext context) {
    if (rating == null) {
      return Padding(
        padding: const EdgeInsets.all(18),
        child: Center(
          child: Text("Không có dữ liệu"),
        ),
      );
    } else {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Đánh giá & bình luận",
            style: TextStyle(
              color: Colors.black87,
              fontSize: 18,
              fontWeight: FontWeight.w500,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: [
              Flexible(
                flex: 1,
                fit: FlexFit.tight,
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      "Đánh giá trung bình",
                      style: TextStyle(
                        color: Colors.black87,
                        fontSize: 14,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    SizedBox(height: 2),
                    Text(
                      "(${rating.data.ratingCount} đánh giá)",
                      style: TextStyle(
                        color: Colors.grey,
                        fontSize: 14,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    SizedBox(height: 2),
                    Text(
                      "${rating.data.avgRating}/5",
                      style: TextStyle(
                        color: Colors.grey,
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
              ),
              Flexible(
                flex: 1,
                fit: FlexFit.tight,
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("5 ", style: TextStyle(color: Colors.grey)),
                        Icon(
                          Icons.star_outlined,
                          size: 16,
                          color: Colors.grey,
                        ),
                        LinearPercentIndicator(
                          percent: rating.data.fiveStarCount == 0
                              ? 0
                              : (rating.data.ratingCount *
                                      100 /
                                      rating.data.fiveStarCount) /
                                  100,
                          width: 90,
                          lineHeight: 10,
                          backgroundColor: Colors.grey,
                          progressColor: Colors.amber,
                        ),
                        Text(
                          "${rating.data.fiveStarCount} đánh giá",
                          style: TextStyle(color: Colors.grey),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("4 ", style: TextStyle(color: Colors.grey)),
                        Icon(
                          Icons.star_outlined,
                          size: 16,
                          color: Colors.grey,
                        ),
                        LinearPercentIndicator(
                          percent: rating.data.fourStarCount == 0
                              ? 0
                              : (rating.data.ratingCount *
                                      100 /
                                      rating.data.fourStarCount) /
                                  100,
                          width: 90,
                          lineHeight: 10,
                          backgroundColor: Colors.grey,
                          progressColor: Colors.amber,
                        ),
                        Text(
                          "${rating.data.fourStarCount} đánh giá",
                          style: TextStyle(color: Colors.grey),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("3 ", style: TextStyle(color: Colors.grey)),
                        Icon(
                          Icons.star_outlined,
                          size: 16,
                          color: Colors.grey,
                        ),
                        LinearPercentIndicator(
                          percent: rating.data.threeStarCount == 0
                              ? 0
                              : (rating.data.ratingCount *
                                      100 /
                                      rating.data.threeStarCount) /
                                  100,
                          width: 90,
                          lineHeight: 10,
                          backgroundColor: Colors.grey,
                          progressColor: Colors.amber,
                        ),
                        Text("${rating.data.threeStarCount} đánh giá",
                            style: TextStyle(color: Colors.grey)),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("2 ", style: TextStyle(color: Colors.grey)),
                        Icon(
                          Icons.star_outlined,
                          size: 16,
                          color: Colors.grey,
                        ),
                        LinearPercentIndicator(
                          percent: rating.data.twoStarCount == 0
                              ? 0
                              : (rating.data.ratingCount *
                                      100 /
                                      rating.data.twoStarCount) /
                                  100,
                          width: 90,
                          lineHeight: 10,
                          backgroundColor: Colors.grey,
                          progressColor: Colors.amber,
                        ),
                        Text("${rating.data.twoStarCount} đánh giá",
                            style: TextStyle(color: Colors.grey)),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("1 ", style: TextStyle(color: Colors.grey)),
                        Icon(
                          Icons.star_outlined,
                          size: 16,
                          color: Colors.grey,
                        ),
                        LinearPercentIndicator(
                          percent: rating.data.oneStarCount == 0
                              ? 0
                              : (rating.data.ratingCount *
                                      100 /
                                      rating.data.oneStarCount) /
                                  100,
                          width: 90,
                          lineHeight: 10,
                          backgroundColor: Colors.grey,
                          progressColor: Colors.amber,
                        ),
                        Text("${rating.data.oneStarCount} đánh giá",
                            style: TextStyle(color: Colors.grey)),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      );
    }
  }
}

class ListAddress extends StatelessWidget {
  const ListAddress({
    Key key,
    @required this.address,
    @required this.isLoading,
  }) : super(key: key);

  final Address address;
  final bool isLoading;

  @override
  Widget build(BuildContext context) {
    if (isLoading) {
      return Center(
        child: CircularProgressIndicator(),
      );
    } else {
      if (address == null) {
        return Center(
          child: Text("Không có dữ liệu"),
        );
      } else {
        return ListView.builder(
          shrinkWrap: true,
          itemCount: address.data.length,
          itemBuilder: (context, index) {
            return ListTile(
              title: Row(
                children: [
                  Icon(
                    Icons.add_location_rounded,
                    color: Colors.red.shade800,
                  ),
                  SizedBox(width: 4),
                  Text(
                    "Kingbuys",
                    style: TextStyle(
                      color: Colors.red.shade800,
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
              subtitle: Row(
                children: [
                  SizedBox(width: 20),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        address.data[index].address,
                        style: TextStyle(color: Colors.black87),
                      ),
                      Row(
                        children: [
                          Icon(
                            Icons.phone,
                            color: Colors.blue,
                            size: 18,
                          ),
                          Text(
                            address.data[index].hotLine,
                            style: TextStyle(color: Colors.blue),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            );
          },
        );
      }
    }
  }
}

class ItemReview extends StatelessWidget {
  const ItemReview({
    Key key,
    @required this.item,
  }) : super(key: key);

  final ReviewItem item;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(100),
            child: CachedNetworkImage(
              imageUrl: AppEndpoint.BASE + item.avatarSource,
              width: 30,
              height: 30,
              fit: BoxFit.cover,
            ),
          ),
          SizedBox(width: 10),
          Flexible(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(item.name),
                    item.isBuy == 0
                        ? Container()
                        : Row(
                            children: [
                              Icon(
                                Icons.check_circle,
                                color: Colors.green,
                                size: 16,
                              ),
                              SizedBox(width: 4),
                              Text(
                                "Đã mua ở đây",
                                style: TextStyle(
                                  color: Colors.green,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ],
                          ),
                    Text(
                      item.phoneNumber.substring(
                            0,
                            item.phoneNumber.length ~/ 2,
                          ) +
                          "*****",
                      style: TextStyle(color: Colors.grey.shade700),
                    ),
                  ],
                ),
                SizedBox(height: 2),
                Text(
                  item.comment,
                  style: TextStyle(
                    color: Colors.black87,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                SizedBox(height: 2),
                Text(
                  DateFormat("d-MM-y").format(item.updatedAt),
                  style: TextStyle(color: Colors.grey.shade600),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class ListGift extends StatelessWidget {
  final List<ProductGift> gifts;

  const ListGift({Key key, this.gifts}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 8),
      color: Colors.white,
      child: Column(
        children: [
          Row(
            children: [
              Image.asset(
                "assets/icons/gift_p.png",
                height: 30,
                width: 30,
              ),
              SizedBox(width: 10),
              Text(
                "Quà tặng kèm: ",
                style: TextStyle(
                  color: Colors.blue,
                  fontSize: 18,
                  fontWeight: FontWeight.w600,
                ),
              )
            ],
          ),
          SizedBox(height: 8),
          ListView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: gifts.length,
            itemBuilder: (context, index) {
              return ItemGift(gift: gifts[index]);
            },
          ),
        ],
      ),
    );
  }
}

class ItemGift extends StatelessWidget {
  const ItemGift({
    Key key,
    @required this.gift,
  }) : super(key: key);

  final ProductGift gift;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        CachedNetworkImage(
          imageUrl: AppEndpoint.BASE + gift.imageSource,
          height: 60,
          width: 60,
          fit: BoxFit.fill,
        ),
        SizedBox(width: 10),
        Flexible(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                gift.name,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  color: Colors.black87,
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                ),
              ),
              Text(
                CustomFormat.formatMoney(gift.price),
                style: TextStyle(
                  color: Colors.red,
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}

class ImageSlideWidget extends StatefulWidget {
  final List<String> item;

  const ImageSlideWidget({Key key, this.item}) : super(key: key);

  @override
  _ImageSlideWidgetState createState() => _ImageSlideWidgetState();
}

class _ImageSlideWidgetState extends State<ImageSlideWidget> {
  CarouselController carouselController;
  int currentImage;

  @override
  void initState() {
    carouselController = CarouselController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        CarouselSlider(
          carouselController: carouselController,
          items: widget.item.map((e) {
            return CachedNetworkImage(
              imageUrl: AppEndpoint.BASE + e,
              fit: BoxFit.fitHeight,
            );
          }).toList(),
          options: CarouselOptions(
            height: 300,
            viewportFraction: 1,
            aspectRatio: 1,
            disableCenter: true,
            onPageChanged: (value, reason) {
              setState(() {
                currentImage = value;
              });
            },
          ),
        ),
        SizedBox(
          height: 100,
          child: ListView.builder(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemCount: widget.item.length,
            itemBuilder: (context, index) {
              return GestureDetector(
                onTap: () => carouselController.jumpToPage(index),
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12),
                    border: Border.all(
                      color: currentImage == index
                          ? Colors.blueAccent
                          : Colors.transparent,
                      width: 4,
                    ),
                  ),
                  margin: EdgeInsets.only(right: 10),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(8),
                    child: CachedNetworkImage(
                      imageUrl: AppEndpoint.BASE + widget.item[index],
                      height: 100,
                      width: 100,
                      fit: BoxFit.fill,
                    ),
                  ),
                ),
              );
            },
          ),
        )
      ],
    );
  }
}
