import 'package:flutter/cupertino.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:hues_kingbuy/src/resource/model/address.dart';
import 'package:hues_kingbuy/src/resource/repository/address_repository.dart';
import 'package:hues_kingbuy/src/screen/base/base.dart';
import 'package:rxdart/rxdart.dart';

class BandoViewmodel extends BaseViewmodel {
  final AddressRepository addressRepository;

  BandoViewmodel(this.addressRepository);

  final _storeAddress = BehaviorSubject<Address>();

  Address get storeAddress => _storeAddress.value;

  set storeAddress(value) {
    _storeAddress.add(value);
    notifyListeners();
  }

  static final CameraPosition _kLake = CameraPosition(
    bearing: 0,
    target: LatLng(10.762622, 106.660172),
    tilt: 0,
    zoom: 14,
  );

  CameraPosition get location => _kLake;

  init() async {
    isLoading = true;
    storeAddress = await addressRepository.getAddressStore();
    isLoading = false;
  }

  Set<Marker> markers() {
    return storeAddress.data.map((e) {
      return Marker(
        markerId: MarkerId(''),
        position: LatLng(
         double.parse(e.latitude),
         double.parse(e.longitude),
        ),
      );
    }).toSet();
  }

  @override
  void dispose() async {
    await _storeAddress.drain();
    _storeAddress.close();
    super.dispose();
  }
}
