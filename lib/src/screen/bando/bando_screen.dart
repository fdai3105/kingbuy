import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:hues_kingbuy/src/config/app_endpoint.dart';
import 'package:hues_kingbuy/src/resource/repository/address_repository.dart';
import 'package:hues_kingbuy/src/screen/bando/bando.dart';
import 'package:hues_kingbuy/src/screen/base/base.dart';

class BandoScreen extends StatelessWidget {
  GoogleMapController mapCtrl;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      extendBody: true,
      appBar: AppBar(
        title: Text('Bản đồ'),
        backgroundColor: Colors.red.shade900.withOpacity(0.8),
        elevation: 0,
      ),
      body: BaseScreen<BandoViewmodel>(
        viewModel: BandoViewmodel(AddressRepository()),
        onViewModelReady: (vm) async {
          vm.init();
        },
        builder: (context, vm, child) {
          if (vm.isLoading) {
            return Center(child: CircularProgressIndicator());
          } else {
            return Stack(
              alignment: Alignment.bottomCenter,
              children: [
                GoogleMap(
                  mapType: MapType.normal,
                  initialCameraPosition: vm.location,
                  onMapCreated: (ctrl) {
                    mapCtrl = ctrl;
                  },
                  markers: vm.markers(),
                  myLocationEnabled: true,
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    height: 200,
                    child: ListView.builder(
                      shrinkWrap: true,
                      physics: BouncingScrollPhysics(),
                      scrollDirection: Axis.horizontal,
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      itemCount: vm.storeAddress.data.length,
                      itemBuilder: (context, index) {
                        final item = vm.storeAddress.data[index];
                        return GestureDetector(
                          onTap: () {
                            mapCtrl.animateCamera(CameraUpdate.newLatLng(
                              LatLng(
                                double.parse(item.latitude),
                                double.parse(item.longitude),
                              ),
                            ));
                          },
                          child: Container(
                            width: 200,
                            margin: EdgeInsets.only(right: 20, bottom: 20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                CachedNetworkImage(
                                  imageUrl: AppEndpoint.BASE + item.imageSource,
                                  height: 100,
                                  width: 200,
                                  fit: BoxFit.cover,
                                ),
                                Container(
                                  padding: EdgeInsets.all(10),
                                  color: Colors.white,
                                  child:
                                      Text("${item.address}\n${item.hotLine}"),
                                )
                              ],
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                ),
              ],
            );
          }
        },
      ),
    );
  }
}
