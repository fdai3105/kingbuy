import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';

import 'base.dart';

class BaseScreen<T extends BaseViewmodel> extends StatefulWidget {
  final Widget Function(BuildContext context, T viewModel, Widget child)
      builder;
  final T viewModel;
  final Widget child;
  final Function(T viewModel) onViewModelReady;

  const BaseScreen(
      {Key key,
      this.builder,
      this.viewModel,
      this.onViewModelReady,
      this.child})
      : super(key: key);

  @override
  _BaseScreenState createState() => _BaseScreenState<T>();
}

class _BaseScreenState<T extends BaseViewmodel> extends State<BaseScreen<T>> {
  @override
  void initState() {
    if (widget.onViewModelReady != null) {
      widget.onViewModelReady(widget.viewModel);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<T>(
      create:(context) => widget.viewModel..setContext(context),
      child: Consumer<T>(
        builder: widget.builder,
        child: widget.child,
      ),
    );
  }
}
