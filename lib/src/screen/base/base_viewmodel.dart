import 'package:flutter/cupertino.dart';
import 'package:rxdart/rxdart.dart';

class BaseViewmodel extends ChangeNotifier {
  BuildContext context;

  final _loadSubject = BehaviorSubject<bool>();

  setContext(BuildContext context) {
    this.context = context;
  }

  bool get isLoading => _loadSubject.value;

  set isLoading(value) {
    _loadSubject.add(value);
    notifyListeners();
  }

  @override
  void dispose() async {
    await _loadSubject.drain();
    _loadSubject.close();
    super.dispose();
  }
}
