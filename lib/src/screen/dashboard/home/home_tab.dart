import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:hues_kingbuy/src/config/config.dart';
import 'package:hues_kingbuy/src/resource/model/model.dart';
import 'package:hues_kingbuy/src/resource/repository/repository.dart';
import 'package:hues_kingbuy/src/screen/base/base.dart';
import 'package:hues_kingbuy/src/screen/dashboard/home/home.dart';
import 'package:hues_kingbuy/src/screen/widget/widget.dart';
import 'package:hues_kingbuy/src/utils/utils.dart';

class HomeTab extends StatefulWidget {
  @override
  _HomeTabState createState() => _HomeTabState();
}

class _HomeTabState extends State<HomeTab> with AutomaticKeepAliveClientMixin {
  ScrollController _scrollController;
  int fetchTime = 0;

  @override
  void initState() {
    _scrollController = ScrollController();
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        print('reach to bottom');
        fetchTime++;
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Container(
      color: Colors.white,
      child: BaseScreen<HomeViewModel>(
        viewModel: HomeViewModel(homeRepository: ProductRepository()),
        onViewModelReady: (vm) async {
          await vm.init();
        },
        builder: (context, vm, child) {
          if (vm.isLoading) {
            return Center(child: CircularProgressIndicator());
          } else {
            return SingleChildScrollView(
              controller: _scrollController,
              child: Column(
                children: [
                  HomeBannerWidget(promotion: vm.promotion),
                  SizedBox(height: 10),
                  DanhMucHotWidget(hotCategory: vm.hotCategory),
                  SizedBox(height: 10),
                  MyPromotionWidget(myPromotion: vm.myPromotion),
                  SizedBox(height: 10),
                  HomeNewProductWidget(product: vm.newProduct),
                  SizedBox(height: 10),
                  HomeSellingProductWidget(product: vm.sellingProduct),
                  SizedBox(height: 10),
                  PreviewCategory(vm: vm),
                  SizedBox(height: 10),
                ],
              ),
            );
          }
        },
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}

class PreviewCategory extends StatelessWidget {
  final HomeViewModel vm;

  const PreviewCategory({
    Key key,
    @required this.vm,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (vm.allCategories == null) {
      return Center(child: Text('Không có dữ liệu'));
    }
    return ListView.builder(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemCount: vm.allCategories.data.categories.length,
      itemBuilder: (context, index) {
        final item = vm.allCategories.data.categories[index];
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: Text(
                item.name ?? "null",
                style: TextStyle(
                  color: Colors.black87,
                  fontSize: 24,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
            SizedBox(height: 8),
            CachedNetworkImage(
              imageUrl: AppEndpoint.BASE + item.backgroundImage,
              width: MediaQuery.of(context).size.width,
              placeholder: (context, string) => CircularProgressIndicator(),
            ),
            SizedBox(height: 16),
            ListViewSelectedWidget(
              initValue: item.id,
              onChanged: (value) => vm.changed(index, value),
              item: item.children,
              padding: EdgeInsets.symmetric(horizontal: 10),
            ),
            SizedBox(height: 16),
            SizedBox(
              height: 270,
              child: vm.getChanged(index).data.length == 0
                  ? Center(child: Text("Không có sản phẩm"))
                  : ListView.builder(
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      itemCount: vm.getChanged(index).data.length,
                      itemBuilder: (context, itemIndex) {
                        return ItemProduct(
                          onTap: () {
                            Navigator.pushNamed(
                              context,
                              Routes.productScreen,
                              arguments: {
                                "product": vm.getChanged(index).data[itemIndex]
                              },
                            );
                          },
                          item: vm.getChanged(index).data[itemIndex],
                        );
                      },
                    ),
            ),
            SizedBox(height: 10),
            Center(
              child: Text(
                "Xem thêm",
                style: TextStyle(
                    color: Colors.blueAccent,
                    fontSize: 14,
                    fontWeight: FontWeight.bold,
                    decoration: TextDecoration.underline),
              ),
            ),
            SizedBox(height: 20),
          ],
        );
      },
    );
  }
}

class HomeNewProductWidget extends StatelessWidget {
  final Product product;

  const HomeNewProductWidget({
    Key key,
    @required this.product,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (product == null) {
      return Center(child: Text('Không có dữ liệu'));
    }

    final items = product.data;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Text(
            "Sản phẩm mới",
            style: TextStyle(
              color: Colors.black87,
              fontSize: 18,
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
        SizedBox(height: 6),
        SizedBox(
          height: 270,
          child: ListView.builder(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            padding: EdgeInsets.symmetric(horizontal: 10),
            itemCount: items.length,
            itemBuilder: (context, index) => ItemProduct(
              onTap: () {
                Navigator.pushNamed(
                  context,
                  Routes.productScreen,
                  arguments: {
                    "product": items[index],
                  },
                );
              },
              item: items[index],
            ),
          ),
        )
      ],
    );
  }
}

class HomeSellingProductWidget extends StatelessWidget {
  final Product product;

  const HomeSellingProductWidget({
    Key key,
    @required this.product,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (product == null) {
      return Center(child: Text('Không có dữ liệu'));
    }
    final items = product.data;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Text(
            "Sản phẩm bán chạy",
            style: TextStyle(
              color: Colors.black87,
              fontSize: 18,
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
        SizedBox(height: 6),
        SizedBox(
          height: 270,
          child: ListView.builder(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            padding: EdgeInsets.symmetric(horizontal: 10),
            itemCount: items.length,
            itemBuilder: (context, index) => ItemProduct(
              onTap: () {
                Navigator.pushNamed(
                  context,
                  Routes.productScreen,
                  arguments: {"product": items[index]},
                );
              },
              item: items[index],
            ),
          ),
        )
      ],
    );
  }
}

class HomeBannerWidget extends StatefulWidget {
  final Promotion promotion;

  const HomeBannerWidget({Key key, this.promotion}) : super(key: key);

  @override
  _HomeBannerWidgetState createState() => _HomeBannerWidgetState();
}

class _HomeBannerWidgetState extends State<HomeBannerWidget> {
  var _index = 0;

  @override
  Widget build(BuildContext context) {
    if (widget.promotion == null) {
      return Center(child: Text('Không có dữ liệu'));
    }
    final news = widget.promotion.data.items;
    return Stack(
      children: [
        CarouselSlider.builder(
          itemCount: news.length,
          itemBuilder: (context, index) {
            final _item = news[index];
            return Container(
              width: MediaQuery.of(context).size.width,
              child: Image.network(
                AppEndpoint.BASE + _item.imageSource,
                fit: BoxFit.fill,
              ),
            );
          },
          options: CarouselOptions(
              initialPage: 0,
              height: 200,
              autoPlay: true,
              autoPlayInterval: Duration(seconds: 4),
              enableInfiniteScroll: true,
              viewportFraction: 1,
              onPageChanged: (index, reason) {
                setState(() {
                  _index = index;
                });
              }),
        ),
        Positioned(
          bottom: 0,
          right: 0,
          child: Row(
            children: news.map((e) {
              final current = news.indexOf(e);
              return Container(
                height: 12,
                width: 12,
                margin: EdgeInsets.only(right: 4, bottom: 4),
                decoration: BoxDecoration(
                  color: current == _index ? Colors.white : Colors.transparent,
                  shape: BoxShape.circle,
                  border: Border.all(color: Colors.white),
                ),
              );
            }).toList(),
          ),
        ),
      ],
    );
  }
}

class DanhMucHotWidget extends StatelessWidget {
  final Category hotCategory;

  const DanhMucHotWidget({
    Key key,
    @required this.hotCategory,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (hotCategory == null) {
      return Center(child: Text('Không có dữ liệu'));
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Text(
            "Danh mục hot",
            style: TextStyle(
              color: Colors.black87,
              fontSize: 18,
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
        SizedBox(height: 6),
        GridView.builder(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          padding: EdgeInsets.symmetric(horizontal: 10),
          itemCount: hotCategory.data.categories.length,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 4,
              mainAxisSpacing: 26,
              crossAxisSpacing: 26,
              childAspectRatio: 1 / 1.8),
          itemBuilder: (context, index) {
            final _item = hotCategory.data.categories[index];
            return GestureDetector(
              onTap: () => Navigator.pushNamed(
                context,
                Routes.categoryScreen,
                arguments: {'category' : _item},
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Image.network(
                    AppEndpoint.BASE + _item.imageSource,
                    height: 80,
                    width: 80,
                  ),
                  SizedBox(height: 4),
                  Text(
                    _item.name,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.black87,
                      fontWeight: FontWeight.w500,
                      fontSize: 14,
                    ),
                    overflow: TextOverflow.fade,
                  ),
                ],
              ),
            );
          },
        )
      ],
    );
  }
}

class MyPromotionWidget extends StatelessWidget {
  final MyPromotion myPromotion;

  const MyPromotionWidget({
    Key key,
    @required this.myPromotion,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (myPromotion == null) {
      return Center(child: Text('Không có dữ liệu'));
    }
    final promotion = myPromotion.data.promotions;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Text(
            "Ưu đãi dành cho bạn",
            style: TextStyle(
              color: Colors.black87,
              fontSize: 18,
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
        SizedBox(height: 6),
        SizedBox(
          height: 160,
          child: ListView.builder(
            shrinkWrap: true,
            padding: EdgeInsets.symmetric(horizontal: 10),
            scrollDirection: Axis.horizontal,
            itemCount: promotion.length,
            itemBuilder: (context, index) {
              return Container(
                margin: EdgeInsets.only(right: 20),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: Image.network(
                    AppEndpoint.BASE + promotion[index].imageSource,
                    fit: BoxFit.fitHeight,
                  ),
                ),
              );
            },
          ),
        )
      ],
    );
  }
}
