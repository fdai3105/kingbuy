import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hues_kingbuy/src/resource/model/model.dart';
import 'package:hues_kingbuy/src/resource/repository/repository.dart';
import 'package:hues_kingbuy/src/screen/base/base.dart';
import 'package:rxdart/rxdart.dart';

class HomeViewModel extends BaseViewmodel {
  final ProductRepository homeRepository;
  final _promotion = BehaviorSubject<Promotion>();
  final _hotCategory = BehaviorSubject<Category>();
  final _myPromotion = BehaviorSubject<MyPromotion>();
  final _newProduct = BehaviorSubject<Product>();
  final _sellingProduct = BehaviorSubject<Product>();
  final _allCategories = BehaviorSubject<Category>();
  final _previewGheMassage = BehaviorSubject<Product>();
  final _previewMayChayBo = BehaviorSubject<Product>();
  final _previewTDTH = BehaviorSubject<Product>();
  final _previewMayMassage = BehaviorSubject<Product>();

  HomeViewModel({@required this.homeRepository});

  Promotion get promotion => _promotion.value;

  set promotion(value) {
    _promotion.add(value);
    notifyListeners();
  }

  Category get hotCategory => _hotCategory.value;

  set hotCategory(value) {
    _hotCategory.add(value);
    notifyListeners();
  }

  MyPromotion get myPromotion => _myPromotion.value;

  set myPromotion(value) {
    _myPromotion.add(value);
    notifyListeners();
  }

  get newProduct => _newProduct.value;

  set newProduct(value) {
    _newProduct.add(value);
    notifyListeners();
  }

  Product get sellingProduct => _sellingProduct.value;

  set sellingProduct(value) {
    _sellingProduct.add(value);
    notifyListeners();
  }

  Category get allCategories => _allCategories.value;

  set allCategories(value) {
    _allCategories.add(value);
    notifyListeners();
  }

  Product get previewGheMassage => _previewGheMassage.value;

  set previewGheMassage(value) {
    _previewGheMassage.add(value);
    notifyListeners();
  }

  Product get previewMayChayBo => _previewMayChayBo.value;

  set previewMayChayBo(value) {
    _previewMayChayBo.add(value);
    notifyListeners();
  }

  Product get previewTDTH => _previewTDTH.value;

  set previewTDTH(value) {
    _previewTDTH.add(value);
    notifyListeners();
  }

  Product get previewMayMassage => _previewMayMassage.value;

  set previewMayMassage(value) {
    _previewMayMassage.add(value);
    notifyListeners();
  }

  init() async {
    isLoading = true;
    promotion = await homeRepository.getPromotion();
    hotCategory = await homeRepository.getHotCategory();
    myPromotion = await homeRepository.getMyPromotion();
    newProduct = await homeRepository.getNewProduct();
    sellingProduct = await homeRepository.getSellingProduct();
    allCategories = await homeRepository.getAllCategories(limit: 4);
    await changedGheMassage(allCategories.data.categories[0].id);
    await changeMayChayBo(allCategories.data.categories[1].id);
    await changeTDTH(allCategories.data.categories[2].id);
    await changeMayMassage(allCategories.data.categories[3].id);
    isLoading = false;
  }

  Future changedGheMassage(dynamic id) async {
    previewGheMassage =
        await homeRepository.getProductsByCategory(categoryID: id);
  }

  Future changeMayChayBo(dynamic id) async {
    previewMayChayBo =
        await homeRepository.getProductsByCategory(categoryID: id);
  }

  Future changeTDTH(dynamic id) async {
    previewTDTH = await homeRepository.getProductsByCategory(categoryID: id);
  }

  Future changeMayMassage(dynamic id) async {
    previewMayMassage =
        await homeRepository.getProductsByCategory(categoryID: id);
  }

  void changed(int index, dynamic value) {
    switch (index) {
      case 0:
        changedGheMassage(value);
        break;
      case 1:
        changeMayChayBo(value);
        break;
      case 2:
        changeTDTH(value);
        break;
      case 3:
        changeMayMassage(value);
        break;
    }
  }

  Product getChanged(int index) {
    switch (index) {
      case 0:
        return previewGheMassage;
        break;
      case 1:
        return previewMayChayBo;
        break;
      case 2:
        return previewTDTH;
        break;
      case 3:
        return previewMayMassage;
        break;
      default:
        return null;
    }
  }

  @override
  void dispose() async {
    await _promotion.drain();
    _promotion.close();
    await _hotCategory.drain();
    _hotCategory.close();
    await _myPromotion.close();
    _myPromotion.close();
    await _newProduct.drain();
    _newProduct.close();
    await _sellingProduct.close();
    _sellingProduct.close();
    await _allCategories.drain();
    _allCategories.close();
    await _previewMayMassage.drain();
    _previewMayMassage.close();
    await _previewTDTH.close();
    _previewTDTH.close();
    await _previewMayChayBo.drain();
    _previewMayChayBo.close();
    await _previewGheMassage.drain();
    _previewGheMassage.close();
    super.dispose();
  }
}
