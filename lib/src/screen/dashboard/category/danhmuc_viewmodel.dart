import 'package:flutter/cupertino.dart';
import 'package:hues_kingbuy/src/resource/model/model.dart';
import 'package:hues_kingbuy/src/resource/repository/repository.dart';
import 'package:hues_kingbuy/src/screen/base/base_viewmodel.dart';
import 'package:rxdart/rxdart.dart';

class DanhMucViewmodel extends BaseViewmodel {
  final ProductRepository homeRepository;

  DanhMucViewmodel({this.homeRepository});

  final _pageCtrl = BehaviorSubject<PageController>();
  final _allCategories = BehaviorSubject<Category>();
  final _currentPage = BehaviorSubject<int>();

  Category get allCategories => _allCategories.value;

  set allCategories(value) {
    _allCategories.add(value);
    notifyListeners();
  }

  int get currentPage => _currentPage.value;

  set currentPage(value) {
    _currentPage.add(value);
    notifyListeners();
  }

  PageController get pageCtrl => _pageCtrl.value;

  set pageCtrl(value) {
    _pageCtrl.add(value);
  }

  init() async {
    isLoading = true;
    allCategories = await homeRepository.getAllCategories();
    pageCtrl = PageController(
      keepPage: true,
      initialPage: 0,
    );
    currentPage = 0;
    isLoading = false;
  }

  void changedCategory(int page) {
    pageCtrl.jumpToPage(page);
    currentPage = page;
  }

  @override
  void dispose() async {
    await _pageCtrl.drain();
    _pageCtrl.close();
    await _allCategories.drain();
    _allCategories.close();
    await _currentPage.drain();
    _currentPage.close();
    super.dispose();
  }
}
