import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:hues_kingbuy/src/config/config.dart';
import 'package:hues_kingbuy/src/resource/model/model.dart';
import 'package:hues_kingbuy/src/resource/repository/repository.dart';
import 'package:hues_kingbuy/src/screen/base/base_screen.dart';
import 'package:hues_kingbuy/src/utils/utils.dart';
import 'danhmuc.dart';

class DanhMucTab extends StatefulWidget {
  @override
  _DanhMucTabState createState() => _DanhMucTabState();
}

class _DanhMucTabState extends State<DanhMucTab> with AutomaticKeepAliveClientMixin{
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Container(
      color: Colors.white,
      child: BaseScreen<DanhMucViewmodel>(
        viewModel: DanhMucViewmodel(homeRepository: ProductRepository()),
        onViewModelReady: (vm) async {
          await vm.init();
        },
        builder: (context, vm, child) {
          if (vm.isLoading) {
            return Center(child: CircularProgressIndicator());
          } else {
            if (vm.allCategories.data.categories.length == 0) {
              return Center(
                child: Text("Không có dữ liệu"),
              );
            } else {
              return Row(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Flexible(
                    flex: 1,
                    child: Container(
                      height: double.infinity,
                      decoration: BoxDecoration(
                        border:
                        Border(right: BorderSide(color: Colors.black12)),
                      ),
                      child: ListChildCategory(vm: vm),
                    ),
                  ),
                  Flexible(
                    flex: 3,
                    fit: FlexFit.loose,
                    child: PageView.builder(
                      controller: vm.pageCtrl,
                      scrollDirection: Axis.vertical,
                      itemCount: vm.allCategories.data.categories.length,
                      onPageChanged: (page) {
                        vm.currentPage = page;
                      },
                      itemBuilder: (context, index) {
                        return Padding(
                          padding: const EdgeInsets.only(
                            top: 10,
                            left: 4,
                            right: 4,
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                vm.allCategories.data.categories[index].name,
                                style: TextStyle(
                                  color: Colors.black87,
                                  fontSize: 20,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                              CachedNetworkImage(
                                imageUrl: AppEndpoint.BASE +
                                    vm.allCategories.data.categories[index]
                                        .backgroundImage,
                                progressIndicatorBuilder:
                                    (context, url, progress) {
                                  return CircularProgressIndicator(
                                      value: progress.progress);
                                },
                              ),
                              GridView(
                                shrinkWrap: true,
                                physics: NeverScrollableScrollPhysics(),
                                gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 3,
                                  childAspectRatio: 1 / 1.2,
                                ),
                                children: _listChildCategories(
                                  context,
                                  vm.allCategories.data.categories[index],
                                ),
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                  )
                ],
              );
            }
          }
        },
      ),
    );
  }

  List<Widget> _listChildCategories(BuildContext context, CategoryItem items) {
    final widgets = <Widget>[];
    for (final item in items.children) {
      final widget = ListCategoryItem(
        onTap: () {
          Navigator.pushNamed(context, Routes.categoryScreen,
              arguments: {"category": item});
        },
        item: item,
      );
      widgets.add(widget);
    }
    return widgets;
  }

  @override
  bool get wantKeepAlive => true;
}

class ListChildCategory extends StatefulWidget {
  final DanhMucViewmodel vm;
  final int initialValue;

  const ListChildCategory({Key key, this.vm, this.initialValue})
      : super(key: key);

  @override
  _ListChildCategoryState createState() => _ListChildCategoryState();
}

class _ListChildCategoryState extends State<ListChildCategory> {
  @override
  void initState() {
    if (widget.initialValue != null) {
      widget.vm.currentPage = widget.initialValue;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final listCategories = widget.vm.allCategories.data.categories;
    return ListView.builder(
      shrinkWrap: true,
      itemCount: listCategories.length,
      itemBuilder: (context, index) {
        return ListCategoryItem(
          item: listCategories[index],
          isSelected: widget.vm.currentPage == index,
          onTap: () {
            widget.vm.changedCategory(index);
          },
        );
      },
    );
  }
}

class ListCategoryItem extends StatelessWidget {
  final CategoryItem item;
  final Function onTap;
  final bool isSelected;

  const ListCategoryItem({
    Key key,
    @required this.item,
    this.isSelected = false,
    @required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        color: isSelected ? Colors.blueAccent : Colors.transparent,
        padding: const EdgeInsets.symmetric(vertical: 10),
        child: Column(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: CachedNetworkImage(
                imageUrl: AppEndpoint.BASE + item.imageSource,
                placeholder: (context, value) {
                  return Image.asset("assets/images/placeholder.png");
                },
                fit: BoxFit.fill,
                height: 60,
                width: 60,
              ),
            ),
            SizedBox(height: 6),
            Text(
              item.name,
              textAlign: TextAlign.center,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
          ],
        ),
      ),
    );
  }
}
