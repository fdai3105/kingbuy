import 'package:hues_kingbuy/src/resource/model/model.dart';
import 'package:hues_kingbuy/src/resource/repository/repository.dart';
import 'package:hues_kingbuy/src/screen/base/base_viewmodel.dart';
import 'package:hues_kingbuy/src/utils/shared_pref.dart';
import 'package:rxdart/subjects.dart';

class NotificationViewmodel extends BaseViewmodel {
  final UserRepository userRepository;

  NotificationViewmodel({this.userRepository});

  final _notifications = BehaviorSubject<Notification>();

  Notification get notifications => _notifications.value;

  set notifications(value) {
    _notifications.add(value);
    notifyListeners();
  }

  Future<void> init() async {
    isLoading = true;
    notifications = await userRepository.getNotification( SharedPref.getToken());
    isLoading = false;
  }

  @override
  void dispose() async {
    await _notifications.drain();
    _notifications.close();
    super.dispose();
  }
}
