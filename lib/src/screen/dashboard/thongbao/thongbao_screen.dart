import 'package:flutter/material.dart';
import 'package:hues_kingbuy/src/resource/repository/repository.dart';
import 'package:hues_kingbuy/src/screen/base/base_screen.dart';
import 'package:hues_kingbuy/src/screen/dashboard/thongbao/thongbao_viewmodel.dart';
import 'package:hues_kingbuy/src/utils/custom_format.dart';

class ThongBaoScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: BaseScreen<NotificationViewmodel>(
        viewModel: NotificationViewmodel(userRepository: UserRepository()),
        onViewModelReady: (vm) async {
          await vm.init();
        },
        builder: (context, vm, child) {
          if (vm.isLoading) {
            return Center(child: CircularProgressIndicator());
          } else {
            if (vm.notifications.data.recordsTotal == 0) {
              return Center(child: Text("Bạn không có thông báo nào"));
            } else {
              return ListView.builder(
                itemCount: vm.notifications.data.notifications.length,
                itemBuilder: (context, index) {
                  final item = vm.notifications.data.notifications[index];
                  return ListTile(
                    title: Text(item.title),
                    subtitle: Text(CustomFormat.formatDate(item.updatedAt)),
                  );
                },
              );
            }
          }
        },
      ),
    );
  }
}
