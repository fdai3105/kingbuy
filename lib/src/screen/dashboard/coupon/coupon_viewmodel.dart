import 'package:hues_kingbuy/src/resource/model/model.dart';
import 'package:hues_kingbuy/src/resource/repository/promotion_repository.dart';
import 'package:hues_kingbuy/src/screen/base/base.dart';
import 'package:hues_kingbuy/src/utils/shared_pref.dart';
import 'package:rxdart/rxdart.dart';

class CouponViewmodel extends BaseViewmodel {
  final PromotionRepository promotionRepository;

  CouponViewmodel({this.promotionRepository});

  final _myCoupon = BehaviorSubject<Coupon>();
  final _card = BehaviorSubject<String>();

  Coupon get myCoupon => _myCoupon.value;

  set myCoupon(value) {
    _myCoupon.add(value);
    notifyListeners();
  }

  String get card => _card.value;

  set card(value) {
    _card.add(value);
    notifyListeners();
  }

  Future<void> init() async {
    isLoading = true;
    myCoupon =
        await promotionRepository.getMyCoupon( SharedPref.getToken());
    final user =  SharedPref.getUser();
    card = user.data.memberCardNumber;
    isLoading = false;
  }

  @override
  void dispose() async {
    await _myCoupon.drain();
    _myCoupon.close();
    await _card.drain();
    _card.close();
    super.dispose();
  }
}
