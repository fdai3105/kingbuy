import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hues_kingbuy/src/resource/model/model.dart';
import 'package:hues_kingbuy/src/resource/repository/repository.dart';
import 'package:hues_kingbuy/src/screen/base/base.dart';
import 'package:hues_kingbuy/src/screen/dashboard/coupon/coupon.dart';
import 'package:hues_kingbuy/src/screen/widget/widget.dart';
import 'package:hues_kingbuy/src/utils/utils.dart';

class CouponScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.symmetric(horizontal: 10),
      child: BaseScreen<CouponViewmodel>(
        viewModel: CouponViewmodel(
          promotionRepository: PromotionRepository(),
        ),
        onViewModelReady: (vm) async {
          await vm.init();
        },
        builder: (context, vm, child) {
          if (vm.isLoading) {
            return Center(child: CircularProgressIndicator());
          } else {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 10),
                Container(
                  width: double.infinity,
                  padding: EdgeInsets.all(20),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(color: Colors.red.shade700, width: 10),
                  ),
                  child: Text(
                    vm.card,
                    textAlign: TextAlign.center,
                    style: TextStyle(letterSpacing: 4),
                  ),
                ),
                SizedBox(height: 20),
                Text(
                  "Coupon chưa sử dụng (${vm.myCoupon.data.nonUse.count})",
                  style: TextStyle(
                    color: Colors.black87,
                    fontSize: 20,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                ListView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: vm.myCoupon.data.nonUse.count,
                  itemBuilder: (context, index) {
                    return ItemCoupon(item: vm.myCoupon.data.nonUse.rows[index]);
                  },
                ),
                SizedBox(height: 20),
                Center(
                  child: FlatButton(
                    onPressed: () {
                      Navigator.pushNamed(
                        context,
                        Routes.detailCoupon,
                        arguments: {"coupon": vm.myCoupon},
                      );
                    },
                    child: Text("Xem tất cả coupon"),
                    textColor: Colors.red.shade700,
                  ),
                ),
              ],
            );
          }
        },
      ),
    );
  }
}

class DetailCouponDialog extends StatelessWidget {
  const DetailCouponDialog({
    Key key,
    @required this.item,
  }) : super(key: key);

  final CouponItem item;

  @override
  Widget build(BuildContext context) {
    return Dialog(
      elevation: 0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Flexible(
                  child: Text(
                    item.name,
                    style: TextStyle(
                      color: Colors.red.shade700,
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
                IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: Icon(Icons.close),
                ),
              ],
            ),
            SizedBox(height: 6),
            Text(item.description),
          ],
        ),
      ),
    );
  }
}
