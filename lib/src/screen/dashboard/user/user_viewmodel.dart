import 'package:flutter/material.dart';
import 'package:hues_kingbuy/src/resource/model/model.dart';
import 'package:hues_kingbuy/src/screen/base/base.dart';
import 'package:hues_kingbuy/src/utils/shared_pref.dart';
import 'package:hues_kingbuy/src/utils/utils.dart';
import 'package:rxdart/rxdart.dart';

class UserViewmodel extends BaseViewmodel {
  final _auth = BehaviorSubject<bool>();
  final _user = BehaviorSubject<User>();

  bool get auth => _auth.value;

  set auth(value) {
    _auth.add(value);
    notifyListeners();
  }

  User get user => _user.value;

  set user(value) {
    _user.add(value);
    notifyListeners();
  }

  Future<void> init() async {
    isLoading = true;
    if ( SharedPref.getToken() == null) {
      auth = false;
    } else {
      auth = true;
    }
    user = SharedPref.getUser();
    isLoading = false;
  }

  Future<void> logout() async {
    await SharedPref.clear();
    Navigator.pushReplacementNamed(context, Routes.dashboardScreen);
  }

  @override
  void dispose() async {
    await _auth.drain();
    _auth.close();
    await _user.drain();
    _user.close();
    super.dispose();
  }
}
