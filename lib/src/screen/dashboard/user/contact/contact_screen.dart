import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hues_kingbuy/src/resource/model/model.dart';
import 'package:hues_kingbuy/src/resource/repository/repository.dart';

class ContactScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Liên hệ"),
      ),
      body: SafeArea(
        child: FutureBuilder<Contact>(
          future: UserRepository().getContacts(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Center(
                child: CircularProgressIndicator(),
              );
            } else {
              if (snapshot.data == null) {
                return Center(
                  child: Text("Không có liên hệ nào cả"),
                );
              } else {
                return Padding(
                  padding: const EdgeInsets.all(8),
                  child: Column(
                    children: [
                      ListTile(
                        leading: Image.asset(
                            "assets/icons/chat-bubbles-with-ellipsis.png"),
                        title: Text(snapshot.data.data[0].email),
                        trailing: Icon(Icons.keyboard_arrow_right_rounded),
                      ),
                      SizedBox(height: 8),
                      ListTile(
                        leading: Image.asset("assets/icons/call-answer.png"),
                        title: Text(snapshot.data.data[0].hotLine),
                        trailing: Icon(Icons.keyboard_arrow_right_rounded),
                      ),
                      SizedBox(height: 8),
                      ListTile(
                        leading: Image.asset("assets/icons/emailOn.png"),
                        title: Text(snapshot.data.data[0].message),
                        trailing: Icon(Icons.keyboard_arrow_right_rounded),
                      ),
                    ],
                  ),
                );
              }
            }
          },
        ),
      ),
    );
  }
}
