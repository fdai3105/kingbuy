import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hues_kingbuy/src/resource/model/model.dart';
import 'package:hues_kingbuy/src/resource/repository/repository.dart';

class CommitmentScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Cam kết của Kingbuy"),
      ),
      body: SafeArea(
        child: FutureBuilder<Commitment>(
          future: UserRepository().getCommitments(),
          builder: (context, snapshot) {
            if(snapshot.connectionState == ConnectionState.waiting) {
              return Center(
                child: CircularProgressIndicator(),
              );
            } else {
              if(snapshot.data == null) {
                return Center(child: Text("Dữ liệu trống"),);
              } else {
                return ListView.builder(
                  itemCount: snapshot.data.data.rows.length,
                  itemBuilder: (context, index) {
                    final item = snapshot.data.data.rows[index];
                    return Container(
                      padding: EdgeInsets.symmetric(
                        horizontal: 20,
                        vertical: 30,
                      ),
                      margin: EdgeInsets.only(bottom: 10),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black26,
                              blurRadius: 3,
                              spreadRadius: 0,
                              offset: Offset(0,2),
                            )
                          ]
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            padding: EdgeInsets.all(14),
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              border: Border.all(color: Colors.black87),
                            ),
                            child: Text(
                              item.shortTitle.split(" ")[0] +
                                  "\n" +
                                  item.shortTitle.split(" ")[1],
                              textAlign: TextAlign.center,
                            ),
                          ),
                          SizedBox(height: 10),
                          Text(
                            item.title,
                            style: TextStyle(
                              color: Colors.red,
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          SizedBox(height: 6),
                          Text(
                            item.description,
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                    );
                  },
                );
              }
            }
          },
        ),
      ),
    );
  }
}
