import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hues_kingbuy/src/config/config.dart';
import 'package:hues_kingbuy/src/screen/base/base.dart';
import 'package:hues_kingbuy/src/screen/dashboard/user/user_viewmodel.dart';
import 'package:hues_kingbuy/src/utils/route.dart';

class UserTab extends StatefulWidget {
  @override
  _UserTabState createState() => _UserTabState();
}

class _UserTabState extends State<UserTab> with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BaseScreen<UserViewmodel>(
      viewModel: UserViewmodel(),
      onViewModelReady: (vm) async {
        await vm.init();
      },
      builder: (context, vm, child) {
        if (vm.isLoading) {
          return Center(child: CircularProgressIndicator());
        }
        return SingleChildScrollView(
          child: Container(
            color: Colors.white,
            child: Column(
              children: [
                vm.auth
                    ? Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Stack(
                            alignment: Alignment.center,
                            fit: StackFit.loose,
                            children: [
                              ClipRRect(
                                borderRadius: BorderRadius.circular(100),
                                child: CachedNetworkImage(
                                  imageUrl: AppEndpoint.BASE +
                                      vm.user.data.profile.avatarSource,
                                  width: 80,
                                  height: 80,
                                  fit: BoxFit.cover,
                                ),
                              ),
                              Positioned(
                                bottom: 0,
                                right: 0,
                                child: Image.asset(
                                  "assets/icons/pencil.png",
                                  width: 25,
                                  height: 25,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 6),
                          Text(
                            vm.user.data.profile.name ?? "",
                            style: TextStyle(
                              color: Colors.black87,
                              fontWeight: FontWeight.w600,
                              fontSize: 18,
                            ),
                          ),
                          SizedBox(height: 4),
                          Text(vm.user.data.profile.phoneNumber ?? ""),
                          SizedBox(height: 4),
                          Text(
                            "${vm.user.data.rewardPoints} điểm" ?? "",
                            style: TextStyle(
                              color: Colors.red,
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ],
                      )
                    : FlatButton(
                        onPressed: () =>
                            Navigator.pushNamed(context, Routes.loginScreen),
                        child: Text("Đăng nhập"),
                        color: Colors.red.shade700,
                        textColor: Colors.white,
                      ),
                ListView(
                  shrinkWrap: true,
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  physics: NeverScrollableScrollPhysics(),
                  children: [
                    ListTile(
                      leading: Image.asset(
                        "assets/icons/e-group.png",
                        height: 30,
                        width: 30,
                      ),
                      title: Text("Thông tin cá nhân"),
                      trailing: Icon(Icons.keyboard_arrow_right),
                    ),
                    ListTile(
                      onTap: () {
                        Navigator.pushNamed(context, Routes.promotionScreen);
                      },
                      leading: Image.asset(
                        "assets/icons/e-discount.png",
                        height: 30,
                        width: 30,
                      ),
                      title: Text("Khuyến mãi"),
                      trailing: Icon(Icons.keyboard_arrow_right),
                    ),
                    ListTile(
                      leading: Image.asset(
                        "assets/icons/e-group-2.png",
                        height: 30,
                        width: 30,
                      ),
                      title: Text("Địa chỉ giao hàng"),
                      trailing: Icon(Icons.keyboard_arrow_right),
                    ),
                    ListTile(
                      leading: Image.asset(
                        "assets/icons/e-path-compound-path.png",
                        height: 30,
                        width: 30,
                      ),
                      title: Text("Coupon của tôi"),
                      trailing: Icon(Icons.keyboard_arrow_right),
                    ),
                    ListTile(
                      leading: Image.asset(
                        "assets/icons/e-path-compound-path-2.png",
                        height: 30,
                        width: 30,
                      ),
                      title: Text("Lịch sử đặt hàng"),
                      trailing: Icon(Icons.keyboard_arrow_right),
                    ),
                    ListTile(
                      leading: Image.asset(
                        "assets/icons/e-viewed.png",
                        height: 30,
                        width: 30,
                      ),
                      title: Text("Sản phẩm đã xem"),
                      trailing: Icon(Icons.keyboard_arrow_right),
                    ),
                    ListTile(
                      leading: Image.asset(
                        "assets/icons/lock.png",
                        height: 30,
                        width: 30,
                      ),
                      title: Text("Đổi mật khẩu"),
                      trailing: Icon(Icons.keyboard_arrow_right),
                    ),
                    ListTile(
                      onTap: () {
                        Navigator.pushNamed(context, Routes.commitmentScreen);
                      },
                      leading: Image.asset(
                        "assets/icons/e-about-us.png",
                        height: 30,
                        width: 30,
                      ),
                      title: Text("Cam kết của Kingbuy"),
                      trailing: Icon(Icons.keyboard_arrow_right),
                    ),
                    ListTile(
                      onTap: () {
                        Navigator.pushNamed(context, Routes.contactScreen);
                      },
                      leading: Image.asset(
                        "assets/icons/phone_call.png",
                        height: 30,
                        width: 30,
                      ),
                      title: Text("Liên hệ"),
                      trailing: Icon(Icons.keyboard_arrow_right),
                    ),
                    ListTile(
                      leading: Image.asset(
                        "assets/icons/e-group-3.png",
                        height: 30,
                        width: 30,
                      ),
                      title: Text("Điều khoản sử dụng"),
                      trailing: Icon(Icons.keyboard_arrow_right),
                    ),
                  ],
                ),
                Column(
                  children: [
                    Text(
                      "Hotline: 1900.6810",
                      style: TextStyle(
                        color: Colors.black87,
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    Text(
                      "2020 Kingbuy Ver 1.0",
                      style: TextStyle(
                        color: Colors.black87,
                      ),
                    ),
                    SizedBox(height: 10),
                    vm.auth
                        ? FlatButton(
                            onPressed: () => vm.logout(),
                            child: Text(
                              "Đăng xuất",
                              style: TextStyle(
                                color: Colors.red,
                                fontSize: 16,
                              ),
                            ),
                          )
                        : Container()
                  ],
                ),
                SizedBox(height: 30),
              ],
            ),
          ),
        );
      },
    );
  }

  @override
  bool get wantKeepAlive => true;
}
