import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hues_kingbuy/src/screen/base/base.dart';
import 'package:hues_kingbuy/src/screen/cart/cart.dart';
import 'package:hues_kingbuy/src/screen/dashboard/coupon/coupon_tab.dart';
import 'package:hues_kingbuy/src/screen/dashboard/dashboard_viewmodel.dart';
import 'package:hues_kingbuy/src/screen/dashboard/user/user.dart';
import 'package:hues_kingbuy/src/screen/widget/widget.dart';
import 'package:hues_kingbuy/src/utils/utils.dart';
import 'package:provider/provider.dart';
import 'category/danhmuc.dart';
import 'home/home.dart';
import 'thongbao/thongbao.dart';

class DashBoardScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final cartQuantity = Provider.of<CartViewmodel>(context).listCart.length;
    return WillPopScope(
      onWillPop: () {
        return SystemNavigator.pop();
      },
      child: BaseScreen<DashBoardViewmodel>(
        viewModel: DashBoardViewmodel(),
        onViewModelReady: (vm) async {
          await vm.init();
        },
        builder: (context, vm, child) {
          return Scaffold(
            appBar: AppBar(
              leading: Image.asset(
                "assets/icons/logo.png",
              ),
              title: GestureDetector(
                onTap: () => Navigator.pushNamed(context, Routes.searchScreen),
                child: Container(
                  padding: EdgeInsets.symmetric(
                    vertical: 6,
                    horizontal: 10,
                  ),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Tìm kiếm...",
                        style: TextStyle(
                          color: Colors.black26,
                          fontSize: 14,
                          fontWeight: FontWeight.normal,
                        ),
                      ),
                      Icon(
                        Icons.search,
                        color: Colors.black26,
                      ),
                    ],
                  ),
                ),
              ),
              actions: [
                IconButton(
                  icon: Icon(Icons.location_on_outlined),
                  onPressed: () => Navigator.pushNamed(context, Routes.bandoScreen),
                ),
                Stack(
                  children: [
                    Align(
                      alignment: Alignment.center,
                      child: IconButton(
                        icon: Icon(Icons.shopping_cart),
                        onPressed: () {
                          Navigator.pushNamed(context, Routes.cartScreen);
                        },
                      ),
                    ),
                    cartQuantity == 0
                        ? Container()
                        : Positioned(
                            top: 2,
                            right: 2,
                            child: Container(
                              padding: EdgeInsets.all(6),
                              decoration: BoxDecoration(
                                color: Colors.red.shade700,
                                shape: BoxShape.circle,
                              ),
                              child: Text(cartQuantity.toString()),
                            ),
                          )
                  ],
                ),
              ],
            ),
            body: PageView(
              controller: vm.pageCtrl,
              children: [
                HomeTab(),
                DanhMucTab(),
                CouponScreen(),
                ThongBaoScreen(),
                UserTab(),
              ],
              onPageChanged: (int) => vm.currentPage = int,
            ),
            bottomNavigationBar: BottomAppBar(
              shape: CircularNotchedRectangle(),
              notchMargin: 6,
              color: Colors.white,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  CustomBottomAppbarItem(
                    onTap: () {
                      vm.jumpTo(0);
                    },
                    icon: "assets/icons/homeOff.png",
                    activeIcon: "assets/icons/homeOn.png",
                    title: "Trang chủ",
                    isActive: vm.currentPage == 0,
                  ),
                  CustomBottomAppbarItem(
                    onTap: () {
                      vm.jumpTo(1);
                    },
                    icon: "assets/icons/menuOff.png",
                    activeIcon: "assets/icons/menuOn.png",
                    title: "Danh mục",
                    isActive: vm.currentPage == 1,
                  ),
                  Container(
                    height: 10,
                    width: 10,
                    margin: EdgeInsets.only(top: 30),
                    decoration: BoxDecoration(
                        color: vm.currentPage == 2
                            ? Colors.red
                            : Colors.transparent,
                        shape: BoxShape.circle),
                  ),
                  CustomBottomAppbarItem(
                    onTap: () async {
                      if (await vm.isLogin()) {
                        vm.jumpTo(3);
                      } else {
                        showDialog(
                            context: context,
                            builder: (context) {
                              return LoginAlertDialog();
                            });
                      }
                    },
                    icon: "assets/icons/alarm.png",
                    activeIcon: "assets/icons/alarmOn.png",
                    title: "Thông báo",
                    isActive: vm.currentPage == 3,
                  ),
                  CustomBottomAppbarItem(
                    onTap: () {
                      vm.jumpTo(4);
                    },
                    icon: "assets/icons/userOff.png",
                    activeIcon: "assets/icons/userOn.png",
                    title: "Tài khoản",
                    isActive: vm.currentPage == 4,
                  )
                ],
              ),
            ),
            floatingActionButton: FloatingActionButton(
              onPressed: () async {
                if (await vm.isLogin()) {
                  vm.jumpTo(2);
                } else {
                  showDialog(
                      context: context,
                      builder: (context) {
                        return LoginAlertDialog();
                      });
                }
              },
              backgroundColor: Colors.red,
              child: Padding(
                padding: const EdgeInsets.all(1),
                child: Container(
                  padding: EdgeInsets.all(12),
                  decoration: BoxDecoration(
                      color: Colors.red,
                      border: Border.all(
                        color: Colors.yellow,
                        width: 2,
                      ),
                      shape: BoxShape.circle),
                  child: Image.asset(
                    "assets/icons/card.png",
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ),
            floatingActionButtonLocation:
                FloatingActionButtonLocation.centerDocked,
          );
        },
      ),
    );
  }
}

class CustomBottomAppbarItem extends StatelessWidget {
  final String icon;
  final String activeIcon;
  final Function onTap;
  final bool isActive;
  final String title;

  const CustomBottomAppbarItem({
    Key key,
    @required this.onTap,
    @required this.icon,
    this.activeIcon,
    this.isActive = false,
    this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      focusColor: Colors.grey,
      borderRadius: BorderRadius.circular(100),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            icon != null
                ? Image.asset(
                    isActive ? activeIcon ?? "" : icon,
                    height: 24,
                    width: 24,
                    gaplessPlayback: true,
                  )
                : SizedBox(),
            SizedBox(height: 2),
            title == null
                ? Container()
                : Text(
                    title,
                    style: TextStyle(
                      color: isActive ? Colors.red : Colors.grey,
                      fontSize: 12,
                    ),
                  ),
          ],
        ),
      ),
    );
  }
}
