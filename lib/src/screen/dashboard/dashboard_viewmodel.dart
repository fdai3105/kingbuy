import 'package:flutter/cupertino.dart';
import 'package:hues_kingbuy/src/screen/base/base_viewmodel.dart';
import 'package:hues_kingbuy/src/utils/shared_pref.dart';
import 'package:rxdart/subjects.dart';

class DashBoardViewmodel extends BaseViewmodel {
  final _pageCtrl = BehaviorSubject<PageController>();
  final _currentPage = BehaviorSubject<int>();

  PageController get pageCtrl => _pageCtrl.value;

  set pageCtrl(value) {
    _pageCtrl.add(value);
    notifyListeners();
  }

  int get currentPage => _currentPage.value;

  set currentPage(value) {
    _currentPage.add(value);
    notifyListeners();
  }

  Future init() async {
    pageCtrl = PageController(
      initialPage: 0,
      keepPage: true,
    );
  }

  void jumpTo(int page) {
    pageCtrl.jumpToPage(page);
    currentPage = page;
  }

  Future<bool> isLogin() async {
    return  SharedPref.getToken() != null;
  }

  @override
  void dispose() async {
    await _pageCtrl.drain();
    _pageCtrl.close();
    await _currentPage.drain();
    _currentPage.close();
    super.dispose();
  }
}
