
import 'package:flutter/material.dart';

class FilterButtonWidget extends StatefulWidget {
  final List<FilterButtonItem> children;
  final Function(dynamic) onSelected;
  final dynamic initialValue;
  final EdgeInsets padding;
  final double mainAxisSpacing;

  const FilterButtonWidget(
      {Key key,
      @required this.children,
      @required this.onSelected,
      this.initialValue,
      this.padding,
      this.mainAxisSpacing})
      : super(key: key);

  @override
  _FilterButtonWidgetState createState() => _FilterButtonWidgetState();
}

class _FilterButtonWidgetState extends State<FilterButtonWidget> {
  dynamic _currentValue;

  @override
  void initState() {
    if (widget.initialValue != null) {
      widget.onSelected(widget.initialValue);
      _currentValue = widget.initialValue;
    } else {
      _currentValue = false;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GridView(
      shrinkWrap: true,
      padding: EdgeInsets.symmetric(
        vertical: 10,
        horizontal: 20,
      ),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          childAspectRatio: 2.8,
          crossAxisSpacing: 20,
          mainAxisSpacing: widget.mainAxisSpacing ?? 10),
      children: _item(),
    );
  }

  List<Widget> _item() {
    final widgets = <Widget>[];
    for (FilterButtonItem child in widget.children) {
      widgets.add(
        Padding(
          padding: widget.padding ?? const EdgeInsets.all(0),
          child: GestureDetector(
            onTap: () {
              setState(() {
                if (_currentValue == child.value) {
                  _currentValue = null;
                  widget.onSelected(null);
                } else {
                  _currentValue = child.value;
                  widget.onSelected(child.value);
                }
              });
            },
            child: Container(
              padding: EdgeInsets.all(2),
              decoration: BoxDecoration(
                color: Colors.lightBlueAccent,
                borderRadius: BorderRadius.circular(8),
                border: Border.all(
                  color: _currentValue == child.value
                      ? Colors.red
                      : Colors.transparent,
                  width: 2,
                ),
              ),
              child: Stack(
                children: [
                  child,
                ],
              ),
            ),
          ),
        ),
      );
    }
    return widgets;
  }
}

class FilterButtonItem extends StatelessWidget {
  final dynamic value;
  final String text;

  const FilterButtonItem({
    Key key,
    this.value,
    this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        text ?? "null",
        textAlign: TextAlign.center,
        overflow: TextOverflow.ellipsis,
        maxLines: 2,
      ),
    );
  }
}
