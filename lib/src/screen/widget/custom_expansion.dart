import 'package:flutter/material.dart';

class CustomExpansion extends StatefulWidget {
  final Widget child;
  final double expandSize;

  const CustomExpansion({
    Key key,
    this.child,
    this.expandSize = 0.2,
  }) : super(key: key);

  @override
  _CustomExpansionState createState() => _CustomExpansionState();
}

class _CustomExpansionState extends State<CustomExpansion>
    with TickerProviderStateMixin {
  bool _isExpanded;
  Animation<double> height;
  AnimationController controller;

  @override
  void initState() {
    _isExpanded = false;
    controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 500),
    );
    height = Tween<double>(begin: widget.expandSize, end: 1).animate(
      CurvedAnimation(
        curve: Curves.easeIn,
        parent: controller,
      ),
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: controller,
      builder: (context, animatedChild) {
        return ClipRect(
          child: Stack(
            children: [
              Align(
                alignment: Alignment.topCenter,
                heightFactor: height.value,
                child: Padding(
                  padding: _isExpanded
                      ? EdgeInsets.only(bottom: 40)
                      : EdgeInsets.zero,
                  child: widget.child,
                ),
              ),
              Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                child: Column(
                  children: [
                    _isExpanded
                        ? Container()
                        : Container(
                            height: 180,
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                begin: Alignment.bottomCenter,
                                end: Alignment.topCenter,
                                colors: [
                                  Colors.white,
                                  Colors.white.withOpacity(0),
                                ],
                                tileMode: TileMode.repeated,
                              ),
                            ),
                          ),
                    Container(
                      color: Colors.white,
                      child: FlatButton(
                        onPressed: () {
                          setState(() {
                            _isExpanded = !_isExpanded;
                          });
                          _isExpanded
                              ? controller.forward()
                              : controller.reverse();
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(_isExpanded ? "Thu gọn" : "Xem thêm"),
                            RotationTransition(
                                turns: Tween(
                                  begin: 0.5,
                                  end: 0.0,
                                ).animate(controller),
                                child: Icon(Icons.keyboard_arrow_up))
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
