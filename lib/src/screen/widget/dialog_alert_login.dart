import 'package:flutter/material.dart';
import 'package:hues_kingbuy/src/utils/utils.dart';

class LoginAlertDialog extends StatelessWidget {
  const LoginAlertDialog({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Container(
        padding: EdgeInsets.all(10),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(height: 6),
            Text(
              "Vui lòng đăng nhập",
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w500,
              ),
            ),
            SizedBox(height: 20),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                FlatButton(
                  onPressed: () => Navigator.pop(context),
                  color: Colors.grey,
                  textColor: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  child: Text("Huỷ bỏ"),
                ),
                FlatButton(
                  onPressed: () =>
                      Navigator.popAndPushNamed(context, Routes.loginScreen),
                  color: Colors.red.shade700,
                  textColor: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  child: Text("Đăng nhập"),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
