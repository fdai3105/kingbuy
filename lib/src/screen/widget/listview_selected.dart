import 'package:flutter/material.dart';
import 'package:hues_kingbuy/src/resource/model/model.dart';

class ListViewSelectedWidget extends StatefulWidget {
  final dynamic initValue;
  final List<CategoryItem> item;
  final Function(dynamic) onChanged;
  final EdgeInsets padding;

  const ListViewSelectedWidget(
      {Key key,
      this.item,
      @required this.onChanged,
      this.initValue,
      this.padding})
      : super(key: key);

  @override
  _ListViewSelectedWidgetState createState() => _ListViewSelectedWidgetState();
}

class _ListViewSelectedWidgetState extends State<ListViewSelectedWidget> {
  int _currentValue;

  @override
  void initState() {
    if(widget.initValue != null) {
      _currentValue = widget.initValue;
      widget.onChanged(widget.initValue);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 40,
      width: double.infinity,
      child: ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        padding: widget.padding ?? const EdgeInsets.all(0),
        itemCount: widget.item.length,
        itemBuilder: (context, index) {
          final childItem = widget.item[index];
          return GestureDetector(
            onTap: () {
              setState(() {
                _currentValue = childItem.id;
              });
              widget.onChanged(childItem.id);
            },
            child: Container(
              width: 120,
              decoration: BoxDecoration(
                color:
                    _currentValue == childItem.id ? Colors.green : Colors.white,
                borderRadius: BorderRadius.circular(6),
              ),
              margin: EdgeInsets.only(right: 10),
              child: Center(
                child: Text(
                  childItem.name,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: _currentValue == childItem.id
                        ? Colors.white
                        : Colors.black87,
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
