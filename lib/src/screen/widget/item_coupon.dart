import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:hues_kingbuy/src/config/config.dart';
import 'package:hues_kingbuy/src/resource/model/model.dart';
import 'package:hues_kingbuy/src/screen/dashboard/coupon/coupon.dart';
import 'package:hues_kingbuy/src/utils/utils.dart';

class ItemCoupon extends StatelessWidget {
  const ItemCoupon({
    Key key,
    @required this.item,
  }) : super(key: key);

  final CouponItem item;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        showDialog(
          context: context,
          builder: (context) {
            return DetailCouponDialog(item: item);
          },
        );
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          CachedNetworkImage(
            imageUrl: AppEndpoint.BASE + item.imageSource,
            height: 100,
            fit: BoxFit.cover,
          ),
          SizedBox(width: 10),
          Flexible(
            fit: FlexFit.tight,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(item.name),
                SizedBox(height: 10),
                Text(
                    "HSD: ${CustomFormat.formatDate(item.expiresAt)}"),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
