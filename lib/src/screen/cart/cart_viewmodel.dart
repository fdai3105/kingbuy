import 'package:hues_kingbuy/src/resource/model/user_address.dart';
import 'package:hues_kingbuy/src/resource/repository/repository.dart';
import 'package:hues_kingbuy/src/screen/base/base.dart';
import 'package:hues_kingbuy/src/resource/model/model.dart';
import 'package:hues_kingbuy/src/utils/shared_pref.dart';
import 'package:rxdart/rxdart.dart';

class CartViewmodel extends BaseViewmodel {
  final ProductRepository productRepository;
  final _listCart = BehaviorSubject<List<Cart>>();

  final _address = BehaviorSubject<UserAddress>();
  final _optionTimeDelivery = BehaviorSubject<int>();
  final _optionCoupon = BehaviorSubject<CouponItem>();
  final _optionPoint = BehaviorSubject<int>();
  final _exportBill = BehaviorSubject<bool>();
  final _gifts = BehaviorSubject<List<ProductGift>>();

  CartViewmodel(this.productRepository);

  List<Cart> get listCart => _listCart.value;

  set listCart(value) {
    _listCart.value.add(value);
    notifyListeners();
  }

  int get total => _totalPrice();

  int get getSale => _salePrice();

  int get optionTimeDelivery => _optionTimeDelivery.value;

  set optionTimeDelivery(value) {
    _optionTimeDelivery.add(value);
    notifyListeners();
  }

  bool get exportBill => _exportBill.value;

  set exportBill(value) {
    _exportBill.add(value);
    notifyListeners();
  }

  UserAddress get address => _address.value;

  set address(value) {
    _address.add(value);
    notifyListeners();
  }

  CouponItem get optionCoupon => _optionCoupon.value;

  set optionCoupon(value) {
    _optionCoupon.add(value);
    notifyListeners();
  }

  int get optionPoint => _optionPoint.value;

  set optionPoint(value) {
    _optionPoint.add(value);
    notifyListeners();
  }

  List<ProductGift> get gifts => _gifts.value;

  set gifts(value) {
    _gifts.add(value);
    notifyListeners();
  }

  init() async {
    isLoading = true;
    _gifts.add(<ProductGift>[]);
    _listCart.add(SharedPref.getCarts());
    optionPoint = SharedPref.getUser().data.rewardPoints;
    exportBill = false;
    isLoading = false;
  }

  void addProduct(ProductItem product) {
    final listProduct = listCart.map((e) => e.product).toList();
    if (listProduct.contains(product)) {
      listCart.firstWhere((element) => element.product == product).quantity++;
    } else {
      listCart = Cart(product: product, quantity: 1);
    }
    _findGift();
    SharedPref.saveCarts(listCart);
    notifyListeners();
  }

  void _findGift() {
    gifts = <ProductGift>[];
    listCart.forEach((element) {
      element.product.gifts.forEach((gift) {
        gifts.add(gift);
      });
    });
  }

  void increaseQuantity(Cart cart) {
    listCart.firstWhere((element) => element == cart).quantity++;
    SharedPref.saveCarts(listCart);
    notifyListeners();
  }

  void decreaseQuantity(Cart cart) {
    final item = listCart.firstWhere((element) => element == cart);
    if (item.quantity > 1) item.quantity--;
    SharedPref.saveCarts(listCart);
    notifyListeners();
  }

  void removeCart(Cart cart) {
    listCart.removeWhere((element) => element == cart);
    if (optionCoupon != null && total < optionCoupon.invoiceTotal) {
      optionCoupon = null;
    }
    _findGift();
    SharedPref.saveCarts(listCart);
    notifyListeners();
  }

  int _totalPrice() {
    int total = 0;
    for (final cart in listCart) {
      total += cart.product.price * cart.quantity;
    }
    return total;
  }

  int _salePrice() {
    if (optionCoupon != null) {
      if ((optionCoupon.percentOff * total / 100) > optionCoupon.maxSale) {
        return total - (total - optionCoupon.maxSale);
      } else {
        return total * (100 - optionCoupon.maxSale) ~/ 100;
      }
    }
    return 0;
  }

  @override
  void dispose() async {
    await _listCart.drain();
    _listCart.close();
    await _optionTimeDelivery.drain();
    _optionTimeDelivery.close();
    await _exportBill.drain();
    _exportBill.close();
    super.dispose();
  }
}
