import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hues_kingbuy/src/config/app_endpoint.dart';
import 'package:hues_kingbuy/src/screen/cart/cart_viewmodel.dart';
import 'package:hues_kingbuy/src/screen/widget/widget.dart';
import 'package:hues_kingbuy/src/utils/custom_format.dart';
import 'package:hues_kingbuy/src/utils/route.dart';
import 'package:hues_kingbuy/src/utils/shared_pref.dart';
import 'package:provider/provider.dart';

class CartScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red.shade700,
        title: Text("Giỏ hàng"),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: ChangeNotifierProvider<CartViewmodel>.value(
            value: Provider.of<CartViewmodel>(context)..setContext(context),
            child: Consumer<CartViewmodel>(
              builder: (context, vm, child) {
                if (vm.isLoading) {
                  return Center(child: CircularProgressIndicator());
                } else {
                  return Container(
                    color: Colors.grey.shade100,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          padding: EdgeInsets.all(10),
                          color: Colors.white,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Danh sách sản phẩm (${vm.listCart.length})',
                                style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                              SizedBox(height: 10),
                              vm.listCart.length == 0
                                  ? Center(child: Text("Không có gì trong giỏ"))
                                  : _ListCart(vm: vm),
                              Divider(),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Thông tin giao hàng',
                                    style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                  GestureDetector(
                                      onTap: () => Navigator.pushNamed(
                                          context, Routes.addressScreen),
                                      behavior: HitTestBehavior.opaque,
                                      child: vm.address == null
                                          ? Text(
                                              'Chọn địa chỉ giao hàng của bạn',
                                              style: TextStyle(
                                                color: Colors.grey,
                                                fontSize: 16,
                                              ),
                                            )
                                          : RichText(
                                              text: TextSpan(
                                                children: [
                                                  TextSpan(
                                                    text: vm.address.fullName,
                                                    style: TextStyle(
                                                      fontSize: 14,
                                                      fontWeight:
                                                          FontWeight.w500,
                                                    ),
                                                  ),
                                                  TextSpan(
                                                    text:
                                                        "\n${vm.address.address}, "
                                                        "${vm.address.ward}, "
                                                        "${vm.address.district}, "
                                                        "${vm.address.city}\n",
                                                  ),
                                                  TextSpan(
                                                    text:
                                                        vm.address.phoneNumber,
                                                  ),
                                                ],
                                                style: TextStyle(
                                                    color: Colors.black87),
                                              ),
                                            ))
                                ],
                              ),
                              Divider(),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Thời gian giao hàng',
                                    style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                  ListView(
                                    shrinkWrap: true,
                                    physics: NeverScrollableScrollPhysics(),
                                    children: [
                                      RadioListTile(
                                        value: 1,
                                        groupValue: vm.optionTimeDelivery,
                                        onChanged: (value) {
                                          vm.optionTimeDelivery = value;
                                        },
                                        title: Text('Giao hàng trong 3h'),
                                        activeColor: Colors.red.shade700,
                                      ),
                                      RadioListTile(
                                        value: 2,
                                        groupValue: vm.optionTimeDelivery,
                                        onChanged: (value) {
                                          vm.optionTimeDelivery = value;
                                        },
                                        title: Text('Giao hàng trong 24h'),
                                        activeColor: Colors.red.shade700,
                                      ),
                                      RadioListTile(
                                        value: 3,
                                        groupValue: vm.optionTimeDelivery,
                                        onChanged: (value) {
                                          vm.optionTimeDelivery = value;
                                        },
                                        title: Text('Nhân viên gọi xác nhận'),
                                        activeColor: Colors.red.shade700,
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                              Divider(),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Ghi chú',
                                    style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                  TextField(
                                    decoration: InputDecoration(
                                      hintText: 'Bạn muốn dặn dò gì không?',
                                      border: InputBorder.none,
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: 10),
                        Container(
                          color: Colors.white,
                          child: CheckboxListTile(
                            title: Text('Yêu cầu hoá đơn'),
                            value: vm.exportBill,
                            onChanged: (value) {
                              vm.exportBill = value;
                            },
                            activeColor: Colors.red.shade700,
                            controlAffinity: ListTileControlAffinity.leading,
                          ),
                        ),
                        SizedBox(height: 10),
                        _OptionsPanel(vm: vm),
                        SizedBox(height: 10),
                        _TotalPanel(vm: vm),
                        SizedBox(height: 10),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: FlatButton(
                            minWidth: double.infinity,
                            color: Colors.red.shade700,
                            textColor: Colors.white,
                            padding: EdgeInsets.symmetric(
                              vertical: 20,
                              horizontal: 30,
                            ),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30)),
                            onPressed: () {},
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  CustomFormat.formatMoney(vm.total + 100000),
                                ),
                                Text('Đặt hàng'),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(height: 10),
                      ],
                    ),
                  );
                }
              },
            ),
          ),
        ),
      ),
    );
  }
}

class _ListCart extends StatelessWidget {
  const _ListCart({
    Key key,
    @required this.vm,
  }) : super(key: key);

  final CartViewmodel vm;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ListView.builder(
          shrinkWrap: true,
          itemCount: vm.listCart.length,
          itemBuilder: (context, index) {
            final item = vm.listCart[index];
            return Row(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                CachedNetworkImage(
                  imageUrl: AppEndpoint.BASE + item.product.imageSource,
                  width: 100,
                  height: 100,
                  fit: BoxFit.cover,
                ),
                SizedBox(width: 10),
                Flexible(
                  fit: FlexFit.tight,
                  child: Container(
                    height: 100,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          item.product.name,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        Text(
                          CustomFormat.formatMoney(item.product.price),
                          style: TextStyle(
                            color: Colors.red.shade700,
                            fontWeight: FontWeight.w600,
                            fontSize: 16,
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            _QuantityButton(
                              quantity: item.quantity.toString(),
                              decrease: () {
                                vm.decreaseQuantity(item);
                              },
                              increase: () {
                                vm.increaseQuantity(item);
                              },
                            ),
                            FlatButton(
                              onPressed: () {
                                vm.removeCart(item);
                              },
                              textColor: Colors.blue,
                              child: Text("Xoá"),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            );
          },
        ),
        SizedBox(height: 10),
        ListView.builder(
          shrinkWrap: true,
          itemCount: vm.gifts.length,
          itemBuilder: (context, index) {
            final gift = vm.gifts[index];
            return Row(
              children: [
                CachedNetworkImage(
                  imageUrl: AppEndpoint.BASE + gift.imageSource,
                  width: 100,
                  height: 100,
                  fit: BoxFit.cover,
                ),
                SizedBox(width: 10),
                Flexible(
                  fit: FlexFit.tight,
                  child: Container(
                    height: 100,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          gift.name,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        Text(
                          CustomFormat.formatMoney(gift.price),
                          style: TextStyle(
                            color: Colors.red.shade700,
                            fontWeight: FontWeight.w600,
                            fontSize: 16,
                            decoration: TextDecoration.lineThrough,
                          ),
                        ),
                        Text(
                          'Tặng kèm',
                          style: TextStyle(
                            color: Colors.purple,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            );
          },
        ),
      ],
    );
  }
}

class _QuantityButton extends StatelessWidget {
  const _QuantityButton({
    Key key,
    @required this.quantity,
    @required this.decrease,
    @required this.increase,
  }) : super(key: key);

  final String quantity;
  final Function decrease;
  final Function increase;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(
            color: Colors.black12,
          ),
          boxShadow: [
            BoxShadow(
              color: Colors.black12,
              blurRadius: 4,
            ),
          ]),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          GestureDetector(
            onTap: decrease,
            behavior: HitTestBehavior.opaque,
            child: Container(
              padding: EdgeInsets.all(10),
              child: Icon(
                Icons.remove,
                size: 12,
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 14),
            decoration: BoxDecoration(
              border: Border.symmetric(
                vertical: BorderSide(
                  color: Colors.black12,
                ),
              ),
            ),
            child: Text(quantity),
          ),
          GestureDetector(
            onTap: increase,
            behavior: HitTestBehavior.opaque,
            child: Container(
              padding: EdgeInsets.all(10),
              child: Icon(
                Icons.add,
                size: 12,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class _OptionsPanel extends StatelessWidget {
  const _OptionsPanel({
    Key key,
    @required this.vm,
  }) : super(key: key);

  final CartViewmodel vm;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
      color: Colors.white,
      child: Column(
        children: [
          GestureDetector(
            onTap: () {
              if (SharedPref.getToken() == null) {
                showDialog(
                    context: context,
                    builder: (context) {
                      return LoginAlertDialog();
                    });
              } else {
                Navigator.pushNamed(
                  context,
                  Routes.cartCouponScreen,
                  arguments: {'total': vm.total},
                );
              }
            },
            behavior: HitTestBehavior.opaque,
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Image.asset(
                        'assets/icons/icCoupon.png',
                        width: 30,
                        height: 30,
                      ),
                      SizedBox(width: 8),
                      Text(
                        'Mã giảm giá',
                        style: TextStyle(fontSize: 14),
                      )
                    ],
                  ),
                  Row(
                    children: [
                      vm.optionCoupon == null
                          ? Text(
                              'Chọn mã giảm giá',
                            )
                          : Text('Đã chọn ${vm.optionCoupon.percentOff}%'),
                      Icon(Icons.keyboard_arrow_right),
                    ],
                  ),
                ],
              ),
            ),
          ),
          Divider(color: Colors.black26, height: 2),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Image.asset(
                      'assets/icons/ic_coin.png',
                      width: 30,
                      height: 30,
                    ),
                    SizedBox(width: 8),
                    Text(
                      'Dùng tích điểm',
                      style: TextStyle(fontSize: 14),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Text('(${vm.optionPoint ?? 0} điểm )'),
                    Switch(
                      value: false,
                      onChanged: (value) {
                      },
                    )
                  ],
                ),
              ],
            ),
          ),
          Divider(color: Colors.black26, height: 2),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Image.asset(
                      'assets/icons/icSurface.png',
                      width: 30,
                      height: 30,
                    ),
                    SizedBox(width: 8),
                    Text(
                      'Phương thức \n thanh toán',
                      style: TextStyle(fontSize: 14),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Text(
                      'Chọn phương thức \n thanh toán',
                      textAlign: TextAlign.end,
                    ),
                    Icon(Icons.keyboard_arrow_right),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class _TotalPanel extends StatelessWidget {
  const _TotalPanel({
    Key key,
    @required this.vm,
  }) : super(key: key);

  final CartViewmodel vm;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: ListView(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        children: [
          ListTile(
            leading: Text(
              'Tạm tính',
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 16,
                  fontWeight: FontWeight.w500),
            ),
            trailing: Text(
              CustomFormat.formatMoney(vm.total),
              style: TextStyle(
                color: Colors.black87,
                fontWeight: FontWeight.w500,
              ),
            ),
            dense: true,
          ),
          ListTile(
            title: Text(
              'Phí vận chuyển',
              style: TextStyle(
                color: Colors.black,
                fontSize: 16,
                fontWeight: FontWeight.w500,
              ),
            ),
            trailing: Text(
              '0d',
              style: TextStyle(
                color: Colors.black87,
                fontWeight: FontWeight.w500,
              ),
            ),
            dense: true,
          ),
          vm.optionCoupon == null
              ? Container()
              : ListTile(
                  title: Text(
                    'Giảm giá',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  trailing: Text(
                    vm.optionCoupon == null
                        ? ''
                        : "-${CustomFormat.formatMoney(vm.getSale)}",
                    style: TextStyle(
                      color: Colors.black87,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  dense: true,
                ),
          Divider(
            color: Colors.black38,
          ),
          ListTile(
            title: Text(
              "Tổng",
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w600,
              ),
            ),
            trailing: Text(
              CustomFormat.formatMoney(vm.total - vm.getSale),
              style: TextStyle(
                color: Colors.red.shade700,
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
