import 'dart:io';

import 'package:dio/dio.dart';
import 'package:hues_kingbuy/src/config/config.dart';
import 'package:hues_kingbuy/src/resource/model/model.dart';
import 'package:hues_kingbuy/src/resource/service/dio_service.dart';

class UserRepository {
  Future<Commitment> getCommitments() async {
    try {
      final response = await DioService().get(AppEndpoint.COMMITMENTS);
      return Commitment.fromJson(response.data);
    } on DioError catch (e) {
      return null;
    }
  }

  Future<Contact> getContacts() async {
    try {
      final response = await DioService().get(AppEndpoint.CONTACT);
      return Contact.fromJson(response.data);
    } on DioError catch (e) {
      return null;
    }
  }

  Future<User> login(String username, String password) async {
    try {
      Map<String, String> params = {"identity": username, "password": password};
      final response = await DioService().post(AppEndpoint.LOGIN, queryParameters: params);
      return User.fromJson(response.data);
    } on DioError catch (e) {
      print(e.response.statusMessage);
      print('UserRepository: ${e.message}');
      return null;
    }
  }

  Future<Notification> getNotification(String token) async {
    try {
      final response = await DioService().get(
        AppEndpoint.NOTIFICATION,
        options: Options(
          headers: {HttpHeaders.authorizationHeader : "Bearer $token"},
        ),
      );
      return Notification.fromJson(response.data);
    } on DioError catch (e) {
      print('getMyCoupon: ${e.error}');
      return null;
    }
  }
}
