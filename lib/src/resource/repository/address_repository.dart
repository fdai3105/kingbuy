import 'package:dio/dio.dart';
import 'package:hues_kingbuy/src/config/config.dart';
import 'package:hues_kingbuy/src/resource/model/Ward.dart';
import 'package:hues_kingbuy/src/resource/model/city.dart';
import 'package:hues_kingbuy/src/resource/model/district.dart';
import 'package:hues_kingbuy/src/resource/model/model.dart';
import 'package:hues_kingbuy/src/resource/service/dio_service.dart';

class AddressRepository {
  Future<Address> getAddressStore() async {
    try {
      final response = await DioService().get(AppEndpoint.ADDRESS);
      return Address.fromJson(response.data);
    } on DioError catch (e) {
      return null;
    }
  }

  Future<City> getCities() async {
    try {
      final response = await DioService().get(AppEndpoint.CITY);
      return City.fromJson(response.data);
    } on DioError catch (e) {
      return null;
    }
  }

  Future<District> getDistrict(dynamic id) async {
    try {
      final response = await DioService().get(AppEndpoint.DISTRICT + "/$id");
      return District.fromJson(response.data);
    } on DioError catch(e) {
      return null;
    }
  }

  Future<Ward> getWards(dynamic id) async {
    try {
      final response = await DioService().get(AppEndpoint.WARD + "/$id");
      return Ward.fromJson(response.data);
    } on DioError catch(e) {
      return null;
    }
  }
}
