import 'package:dio/dio.dart';
import 'package:hues_kingbuy/src/config/config.dart';
import 'package:hues_kingbuy/src/resource/model/model.dart';
import 'package:hues_kingbuy/src/resource/model/purchased_together.dart';
import 'package:hues_kingbuy/src/resource/service/dio_service.dart';

class ProductRepository {
  Future<Promotion> getPromotion() async {
    try {
      Map<String, dynamic> _query = {"limit": 20, "offset": 0};
      final response = await DioService()
          .get(AppEndpoint.PROMOTION, queryParameters: _query);
      return Promotion.fromJson(response.data);
    } on DioError catch (e) {
      print(e.toString());
      return null;
    }
  }

  Future<Category> getHotCategory() async {
    try {
      final response = await DioService().get(
        AppEndpoint.HOT_CATEGORY,
      );
      return Category.fromJson(response.data);
    } on DioError catch (e) {}
    return null;
  }

  Future<MyPromotion> getMyPromotion() async {
    try {
      Map<String, dynamic> query = {"limit": 20, "offset": 0};
      final response = await DioService().get(
        AppEndpoint.MY_PROMOTION,
        queryParameters: query,
      );
      return MyPromotion.fromJson(response.data);
    } on DioError catch (e) {}
    return null;
  }

  Future<Product> getNewProduct({int limit = 20, int offset = 0}) async {
    try {
      Map<String, dynamic> query = {"limit": limit, "offset": offset};
      final response = await DioService()
          .get(AppEndpoint.PRODUCT_NEW, queryParameters: query);
      return Product.fromJson(response.data);
    } on DioError catch (e) {}
    return null;
  }

  Future<Product> getSellingProduct({int limit = 20, int offset = 0}) async {
    try {
      Map<String, dynamic> query = {"limit": limit, "offset": offset};
      final response = await DioService()
          .get(AppEndpoint.PRODUCT_SELLING, queryParameters: query);
      return Product.fromJson(response.data);
    } on DioError catch (e) {}
    return null;
  }

  Future<Category> getAllCategories({int limit = 20, int offset = 0}) async {
    try {
      Map<String, dynamic> query = {"limit": limit, "offset": offset};
      final response = await DioService()
          .get(AppEndpoint.ALL_CATEGORY, queryParameters: query);
      return Category.fromJson(response.data);
    } on DioError catch (e) {}
    return null;
  }

  Future<Product> getProductsByCategory(
      {dynamic categoryID, int limit = 20, int offset = 0}) async {
    try {
      Map<String, dynamic> query = {
        "product_category_id": categoryID,
        "limit": limit,
        "offset": offset,
      };
      final response = await DioService()
          .get(AppEndpoint.PRODUCTS_BY_CATEGORY, queryParameters: query);
      return Product.fromJsonProducts(response.data);
    } on DioError catch (e) {
      return null;
    }
  }

  Future<Product> searchProduct(
      {String keyword = "",
      int limit = 20,
      int offset = 0,
      int category,
      int brand,
      Map<String, dynamic> price}) async {
    Map<String, dynamic> query = {
      "searchWord": keyword,
      "limit": limit,
      "offset": offset,
      "product_category_id": category,
      "brand_id": brand,
      // "price[from]": price == null ? "": price["from"],
      // "price[to]": price["to"] ?? "",
    };
    try {
      final response = await DioService()
          .get(AppEndpoint.SEARCH_PRODUCT, queryParameters: query);
      return Product.fromJson(response.data);
    } on DioError catch (e) {
      print(e.error);
    }
    return null;
  }

  Future<Rating> getRating(int id) async {
    Map<String, dynamic> query = {
      "product_id": id,
    };
    try {
      final response =
          await DioService().get(AppEndpoint.RATING, queryParameters: query);
      return Rating.fromJson(response.data);
    } on DioError catch (e) {
      print(e.error);
    }
    return null;
  }

  Future<Review> getReview(int id) async {
    Map<String, dynamic> query = {
      "product_id": id,
    };
    try {
      final response =
          await DioService().get(AppEndpoint.REVIEW, queryParameters: query);
      return Review.fromJson(response.data);
    } on DioError catch (e) {
      print(e.error);
    }
    return null;
  }

  Future<PurchasedTogether> getPurchasedTogether(int id) async {
    Map<String, dynamic> query = {
      "product_id": id,
      "limit": 10,
      "offset": 0,
    };
    try {
      final response = await DioService()
          .get(AppEndpoint.PURCHAR_TOGETHER, queryParameters: query);
      return PurchasedTogether.fromJson(response.data);
    } on DioError catch (e) {
      print(e.error);
      return null;
    }
  }

  Future<Product> getRelated(int id) async {
    Map<String, dynamic> query = {
      "limit": 10,
      'offset': 0,
    };
    try {
      final response = await DioService()
          .get(AppEndpoint.RELATED + "/$id", queryParameters: query);
      return Product.fromJson(response.data);
    } on DioError catch (e) {
      print(e.error);
      return null;
    }
  }
}
