import 'dart:io';

import 'package:dio/dio.dart';
import 'package:hues_kingbuy/src/config/config.dart';
import 'package:hues_kingbuy/src/resource/model/model.dart';
import 'package:hues_kingbuy/src/resource/service/dio_service.dart';

class PromotionRepository {
  Future<Coupon> getMyCoupon(String token) async {
    try {
      final response = await DioService().get(
        AppEndpoint.MY_COUPONS,
        options: Options(
          headers: {HttpHeaders.authorizationHeader : "Bearer $token"},
        ),
      );
      return Coupon.fromJson(response.data);
    } on DioError catch (e) {
      print('getMyCoupon: ${e.error}');
      return null;
    }
  }
}
