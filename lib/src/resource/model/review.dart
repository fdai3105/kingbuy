import 'dart:convert';

class Review {
  Review({
    this.status,
    this.data,
  });

  final int status;
  final ReviewData data;

  factory Review.fromRawJson(String str) => Review.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Review.fromJson(Map<String, dynamic> json) => Review(
    status: json["status"],
    data: ReviewData.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "data": data.toJson(),
  };
}

class ReviewData {
  ReviewData({
    this.rows,
    this.totalRecords,
  });

  final List<ReviewItem> rows;
  final int totalRecords;

  factory ReviewData.fromRawJson(String str) => ReviewData.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ReviewData.fromJson(Map<String, dynamic> json) => ReviewData(
    rows: List<ReviewItem>.from(json["rows"].map((x) => ReviewItem.fromJson(x))),
    totalRecords: json["total_records"],
  );

  Map<String, dynamic> toJson() => {
    "rows": List<dynamic>.from(rows.map((x) => x.toJson())),
    "total_records": totalRecords,
  };
}

class ReviewItem {
  ReviewItem({
    this.id,
    this.userId,
    this.productId,
    this.name,
    this.phoneNumber,
    this.comment,
    this.status,
    this.createdAt,
    this.updatedAt,
    this.star,
    this.avatarSource,
    this.isBuy,
  });

  final int id;
  final int userId;
  final int productId;
  final String name;
  final String phoneNumber;
  final String comment;
  final int status;
  final DateTime createdAt;
  final DateTime updatedAt;
  final int star;
  final String avatarSource;
  final int isBuy;

  factory ReviewItem.fromRawJson(String str) => ReviewItem.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ReviewItem.fromJson(Map<String, dynamic> json) => ReviewItem(
    id: json["id"],
    userId: json["user_id"],
    productId: json["product_id"],
    name: json["name"],
    phoneNumber: json["phone_number"],
    comment: json["comment"],
    status: json["status"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
    star: json["star"],
    avatarSource: json["avatar_source"],
    isBuy: json["is_buy"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "user_id": userId,
    "product_id": productId,
    "name": name,
    "phone_number": phoneNumber,
    "comment": comment,
    "status": status,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
    "star": star,
    "avatar_source": avatarSource,
    "is_buy": isBuy,
  };
}
