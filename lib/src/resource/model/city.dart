import 'dart:convert';

class City {
  City({
    this.status,
    this.data,
  });

  final int status;
  final List<CityItem> data;

  factory City.fromRawJson(String str) => City.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory City.fromJson(Map<String, dynamic> json) => City(
    status: json["status"],
    data: List<CityItem>.from(json["data"].map((x) => CityItem.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class CityItem {
  CityItem({
    this.code,
    this.name,
  });

  final String code;
  final String name;

  factory CityItem.fromRawJson(String str) => CityItem.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory CityItem.fromJson(Map<String, dynamic> json) => CityItem(
    code: json["code"],
    name: json["name"],
  );

  Map<String, dynamic> toJson() => {
    "code": code,
    "name": name,
  };
}
