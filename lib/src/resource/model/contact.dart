import 'dart:convert';

class Contact {
  Contact({
    this.status,
    this.data,
  });

  final int status;
  final List<ContactItem> data;

  factory Contact.fromRawJson(String str) => Contact.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Contact.fromJson(Map<String, dynamic> json) => Contact(
    status: json["status"],
    data: List<ContactItem>.from(json["data"].map((x) => ContactItem.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class ContactItem {
  ContactItem({
    this.message,
    this.hotLine,
    this.email,
  });

  final String message;
  final String hotLine;
  final String email;

  factory ContactItem.fromRawJson(String str) => ContactItem.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ContactItem.fromJson(Map<String, dynamic> json) => ContactItem(
    message: json["message"],
    hotLine: json["hotLine"],
    email: json["email"],
  );

  Map<String, dynamic> toJson() => {
    "message": message,
    "hotLine": hotLine,
    "email": email,
  };
}
