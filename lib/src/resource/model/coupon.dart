import 'dart:convert';

class Coupon {
  Coupon({
    this.status,
    this.data,
  });

  final int status;
  final CouponData data;

  factory Coupon.fromRawJson(String str) => Coupon.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Coupon.fromJson(Map<String, dynamic> json) => Coupon(
    status: json["status"],
    data: CouponData.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "data": data.toJson(),
  };
}

class CouponData {
  CouponData({
    this.nonUse,
    this.used,
    this.expired,
  });

  final CouponExpired nonUse;
  final CouponExpired used;
  final CouponExpired expired;

  factory CouponData.fromRawJson(String str) => CouponData.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory CouponData.fromJson(Map<String, dynamic> json) => CouponData(
    nonUse: CouponExpired.fromJson(json["non_use"]),
    used: CouponExpired.fromJson(json["used"]),
    expired: CouponExpired.fromJson(json["expired"]),
  );

  Map<String, dynamic> toJson() => {
    "non_use": nonUse.toJson(),
    "used": used.toJson(),
    "expired": expired.toJson(),
  };
}

class CouponExpired {
  CouponExpired({
    this.count,
    this.rows,
  });

  final int count;
  final List<CouponItem> rows;

  factory CouponExpired.fromRawJson(String str) => CouponExpired.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory CouponExpired.fromJson(Map<String, dynamic> json) => CouponExpired(
    count: json["count"],
    rows: List<CouponItem>.from(json["rows"].map((x) => CouponItem.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "count": count,
    "rows": List<dynamic>.from(rows.map((x) => x.toJson())),
  };
}

class CouponItem {
  CouponItem({
    this.id,
    this.name,
    this.description,
    this.applyTarget,
    this.type,
    this.invoiceStatus,
    this.invoiceTotal,
    this.percentOff,
    this.maxSale,
    this.salePrice,
    this.status,
    this.expiresAt,
    this.imageSource,
    this.createdAt,
    this.updatedAt,
  });

  final int id;
  final String name;
  final String description;
  final int applyTarget;
  final int type;
  final int invoiceStatus;
  final int invoiceTotal;
  final int percentOff;
  final int maxSale;
  final dynamic salePrice;
  final int status;
  final DateTime expiresAt;
  final String imageSource;
  final DateTime createdAt;
  final DateTime updatedAt;

  factory CouponItem.fromRawJson(String str) => CouponItem.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory CouponItem.fromJson(Map<String, dynamic> json) => CouponItem(
    id: json["id"],
    name: json["name"],
    description: json["description"],
    applyTarget: json["apply_target"],
    type: json["type"],
    invoiceStatus: json["invoice_status"],
    invoiceTotal: json["invoice_total"],
    percentOff: json["percent_off"] == null ? null : json["percent_off"],
    maxSale: json["max_sale"] == null ? null : json["max_sale"],
    salePrice: json["sale_price"],
    status: json["status"],
    expiresAt: DateTime.parse(json["expires_at"]),
    imageSource: json["image_source"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "description": description,
    "apply_target": applyTarget,
    "type": type,
    "invoice_status": invoiceStatus,
    "invoice_total": invoiceTotal,
    "percent_off": percentOff == null ? null : percentOff,
    "max_sale": maxSale == null ? null : maxSale,
    "sale_price": salePrice,
    "status": status,
    "expires_at": "${expiresAt.year.toString().padLeft(4, '0')}-${expiresAt.month.toString().padLeft(2, '0')}-${expiresAt.day.toString().padLeft(2, '0')}",
    "image_source": imageSource,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
  };
}
