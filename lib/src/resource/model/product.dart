import 'dart:convert';

class Product {
  Product({
    this.status,
    this.data,
  });

  final int status;
  final List<ProductItem> data;

  factory Product.fromRawJson(String str) => Product.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Product.fromJson(Map<String, dynamic> json) => Product(
        status: json["status"],
        data: List<ProductItem>.from(
            json["data"].map((x) => ProductItem.fromJson(x))),
      );

  factory Product.fromJsonProducts(Map<String, dynamic> json) {
    return Product(
      status: json["status"],
      data: List<ProductItem>.from(
        json["data"]["products"].map((x) => ProductItem.fromJson(x)),
      ),
    );
  }

  Map<String, dynamic> toJson() => {
        "status": status,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };

  @override
  String toString() {
    return 'Product{status: $status, data: $data}';
  }
}

class ProductItem {
  ProductItem({
    this.id,
    this.name,
    this.price,
    this.salePrice,
    this.productCategoryId,
    this.category,
    this.brandId,
    this.giftIds,
    this.imageSource,
    this.imageSourceList,
    this.description,
    this.content,
    this.specifications,
    this.videoLink,
    this.slug,
    this.status,
    this.goodsStatus,
    this.isBulky,
    this.isInstallment,
    this.barcode,
    this.createdAt,
    this.updatedAt,
    this.productCategoryName,
    this.brandName,
    this.brandInfo,
    this.saleOff,
    this.gifts,
    this.star,
    this.colors,
  });

  final int id;
  final String name;
  final int price;
  final int salePrice;
  final int productCategoryId;
  final ProductCategory category;
  final int brandId;
  final List<int> giftIds;
  final String imageSource;
  final List<String> imageSourceList;
  final String description;
  final String content;
  final String specifications;
  final String videoLink;
  final String slug;
  final int status;
  final int goodsStatus;
  final int isBulky;
  final int isInstallment;
  final String barcode;
  final String createdAt;
  final String updatedAt;
  final String productCategoryName;
  final String brandName;
  final String brandInfo;
  final int saleOff;
  final List<ProductGift> gifts;
  final int star;
  final List<ProductColor> colors;

  factory ProductItem.fromRawJson(String str) =>
      ProductItem.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ProductItem.fromJson(Map<String, dynamic> json) => ProductItem(
        id: json["id"],
        name: json["name"],
        price: json["price"],
        salePrice: json["sale_price"],
        productCategoryId: json["product_category_id"],
        category: ProductCategory.fromJson(json["category"]),
        brandId: json["brand_id"],
        giftIds: List<int>.from(json["gift_ids"].map((x) => x)),
        imageSource: json["image_source"],
        imageSourceList:
            List<String>.from(json["image_source_list"].map((x) => x)),
        description: json["description"],
        content: json["content"],
        specifications: json["specifications"],
        videoLink: json["video_link"] == null ? null : json["video_link"],
        slug: json["slug"],
        status: json["status"],
        goodsStatus: json["goods_status"],
        isBulky: json["is_bulky"],
        isInstallment: json["is_installment"],
        barcode: json["barcode"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
        productCategoryName: json["product_category_name"],
        brandName: json["brand_name"],
        brandInfo: json["brand_info"] == null ? null : json["brand_info"],
        saleOff: json["sale_off"],
        gifts: List<ProductGift>.from(
            json["gifts"].map((x) => ProductGift.fromJson(x))),
        star: json["star"],
        colors: List<ProductColor>.from(
            json["colors"].map((x) => ProductColor.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "price": price,
        "sale_price": salePrice,
        "product_category_id": productCategoryId,
        "category": category.toJson(),
        "brand_id": brandId,
        "gift_ids": List<dynamic>.from(giftIds.map((x) => x)),
        "image_source": imageSource,
        "image_source_list": List<dynamic>.from(imageSourceList.map((x) => x)),
        "description": description,
        "content": content,
        "specifications": specifications,
        "video_link": videoLink == null ? null : videoLink,
        "slug": slug,
        "status": status,
        "goods_status": goodsStatus,
        "is_bulky": isBulky,
        "is_installment": isInstallment,
        "barcode": barcode,
        "created_at": createdAt,
        "updated_at": updatedAt,
        "product_category_name": productCategoryName,
        "brand_name": brandName,
        "brand_info": brandInfo == null ? null : brandInfo,
        "sale_off": saleOff,
        "gifts": List<dynamic>.from(gifts.map((x) => x.toJson())),
        "star": star,
        "colors": List<dynamic>.from(colors.map((x) => x.toJson())),
      };

  @override
  String toString() {
    return 'ProductDatum{id: $id, name: $name, price: $price, salePrice: $salePrice, productCategoryId: $productCategoryId, category: $category, brandId: $brandId, giftIds: $giftIds, imageSource: $imageSource, imageSourceList: $imageSourceList, description: $description, content: $content, specifications: $specifications, videoLink: $videoLink, slug: $slug, status: $status, goodsStatus: $goodsStatus, isBulky: $isBulky, isInstallment: $isInstallment, barcode: $barcode, createdAt: $createdAt, updatedAt: $updatedAt, productCategoryName: $productCategoryName, brandName: $brandName, brandInfo: $brandInfo, saleOff: $saleOff, gifts: $gifts, star: $star, colors: $colors}';
  }
}

class ProductCategory {
  ProductCategory({
    this.id,
    this.name,
    this.parent,
  });

  final int id;
  final String name;
  final ProductCategory parent;

  factory ProductCategory.fromRawJson(String str) =>
      ProductCategory.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ProductCategory.fromJson(Map<String, dynamic> json) =>
      ProductCategory(
        id: json["id"],
        name: json["name"],
        parent: json["parent"] == null
            ? null
            : ProductCategory.fromJson(json["parent"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "parent": parent == null ? null : parent.toJson(),
      };

  @override
  String toString() {
    return 'ProductCategory{id: $id, name: $name, parent: $parent}';
  }
}

class ProductBrand {
  final int id;
  final String name;
  final String info;

  ProductBrand({this.id, this.name, this.info});

  @override
  String toString() {
    return 'ProductBrand{id: $id, name: $name, info: $info}';
  }
}

class ProductColor {
  ProductColor({
    this.id,
    this.name,
    this.imageSource,
  });

  final int id;
  final String name;
  final List<String> imageSource;

  factory ProductColor.fromRawJson(String str) =>
      ProductColor.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ProductColor.fromJson(Map<String, dynamic> json) => ProductColor(
        id: json["id"],
        name: json["name"],
        imageSource: List<String>.from(json["image_source"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "image_source": List<dynamic>.from(imageSource.map((x) => x)),
      };

  @override
  String toString() {
    return 'ProductColor{id: $id, name: $name, imageSource: $imageSource}';
  }
}

class ProductGift {
  ProductGift({
    this.id,
    this.name,
    this.price,
    this.imageSource,
    this.description,
    this.createdAt,
    this.updatedAt,
  });

  final int id;
  final String name;
  final int price;
  final String imageSource;
  final dynamic description;
  final DateTime createdAt;
  final DateTime updatedAt;

  factory ProductGift.fromRawJson(String str) =>
      ProductGift.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ProductGift.fromJson(Map<String, dynamic> json) => ProductGift(
        id: json["id"],
        name: json["name"],
        price: json["price"],
        imageSource: json["image_source"],
        description: json["description"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "price": price,
        "image_source": imageSource,
        "description": description,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
      };

  @override
  String toString() {
    return 'ProductGift{id: $id, name: $name, price: $price, imageSource: $imageSource, description: $description, createdAt: $createdAt, updatedAt: $updatedAt}';
  }
}
