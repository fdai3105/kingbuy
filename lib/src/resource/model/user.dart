import 'dart:convert';

class User {
  User({
    this.status,
    this.data,
    this.message,
  });

  final int status;
  final UserData data;
  final String message;

  factory User.fromRawJson(String str) => User.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory User.fromJson(Map<String, dynamic> json) => User(
        status: json["status"],
        data: UserData.fromJson(json["data"]),
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "data": data.toJson(),
        "message": message,
      };
}

class UserData {
  UserData({
    this.profile,
    this.token,
    this.memberCardNumber,
    this.rewardPoints,
  });

  final UserItem profile;
  final String token;
  final String memberCardNumber;
  final int rewardPoints;

  factory UserData.fromRawJson(String str) =>
      UserData.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory UserData.fromJson(Map<String, dynamic> json) => UserData(
        profile: UserItem.fromJson(json["profile"]),
        token: json["token"],
        memberCardNumber: json["member_card_number"],
        rewardPoints: json["reward_points"],
      );

  Map<String, dynamic> toJson() => {
        "profile": profile.toJson(),
        "token": token,
        "member_card_number": memberCardNumber,
        "reward_points": rewardPoints,
      };
}

class UserItem {
  UserItem({
    this.id,
    this.name,
    this.email,
    this.phoneNumber,
    this.avatarSource,
    this.dateOfBirth,
    this.gender,
    this.status,
    this.deviceToken,
    this.createdAt,
    this.updatedAt,
  });

  final int id;
  final String name;
  final String email;
  final String phoneNumber;
  final String avatarSource;
  final DateTime dateOfBirth;
  final int gender;
  final int status;
  final String deviceToken;
  final DateTime createdAt;
  final DateTime updatedAt;

  factory UserItem.fromRawJson(String str) =>
      UserItem.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory UserItem.fromJson(Map<String, dynamic> json) => UserItem(
        id: json["id"],
        name: json["name"],
        email: json["email"],
        phoneNumber: json["phone_number"],
        avatarSource: json["avatar_source"],
        dateOfBirth: DateTime.parse(json["date_of_birth"]),
        gender: json["gender"],
        status: json["status"],
        deviceToken: json["device_token"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "email": email,
        "phone_number": phoneNumber,
        "avatar_source": avatarSource,
        "date_of_birth":
            "${dateOfBirth.year.toString().padLeft(4, '0')}-${dateOfBirth.month.toString().padLeft(2, '0')}-${dateOfBirth.day.toString().padLeft(2, '0')}",
        "gender": gender,
        "status": status,
        "device_token": deviceToken,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
      };
}
