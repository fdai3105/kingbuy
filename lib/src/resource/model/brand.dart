import 'dart:convert';

class Brand {
  Brand({
    this.status,
    this.message,
    this.data,
  });

  final int status;
  final String message;
  final BrandData data;

  factory Brand.fromRawJson(String str) => Brand.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Brand.fromJson(Map<String, dynamic> json) => Brand(
    status: json["status"],
    message: json["message"],
    data: BrandData.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "data": data.toJson(),
  };
}

class BrandData {
  BrandData({
    this.brands,
  });

  final List<BrandItem> brands;

  factory BrandData.fromRawJson(String str) => BrandData.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory BrandData.fromJson(Map<String, dynamic> json) => BrandData(
    brands: List<BrandItem>.from(json["brands"].map((x) => BrandItem.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "brands": List<dynamic>.from(brands.map((x) => x.toJson())),
  };
}

class BrandItem {
  BrandItem({
    this.id,
    this.name,
    this.imageSource,
    this.content,
    this.slug,
    this.displayOrder,
    this.createdAt,
    this.updatedAt,
  });

  final int id;
  final String name;
  final String imageSource;
  final String content;
  final String slug;
  final int displayOrder;
  final DateTime createdAt;
  final DateTime updatedAt;

  factory BrandItem.fromRawJson(String str) => BrandItem.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory BrandItem.fromJson(Map<String, dynamic> json) => BrandItem(
    id: json["id"],
    name: json["name"],
    imageSource: json["image_source"],
    content: json["content"] == null ? null : json["content"],
    slug: json["slug"],
    displayOrder: json["display_order"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "image_source": imageSource,
    "content": content == null ? null : content,
    "slug": slug,
    "display_order": displayOrder,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
  };
}
