import 'dart:convert';

class Notification {
  Notification({
    this.status,
    this.data,
  });

  final int status;
  final NotificationData data;

  factory Notification.fromRawJson(String str) => Notification.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Notification.fromJson(Map<String, dynamic> json) => Notification(
    status: json["status"],
    data: NotificationData.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "data": data.toJson(),
  };
}

class NotificationData {
  NotificationData({
    this.notifications,
    this.recordsTotal,
  });

  final List<NotificationItem> notifications;
  final int recordsTotal;

  factory NotificationData.fromRawJson(String str) => NotificationData.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory NotificationData.fromJson(Map<String, dynamic> json) => NotificationData(
    notifications: List<NotificationItem>.from(json["notifications"].map((x) => NotificationItem.fromJson(x))),
    recordsTotal: json["recordsTotal"],
  );

  Map<String, dynamic> toJson() => {
    "notifications": List<dynamic>.from(notifications.map((x) => x.toJson())),
    "recordsTotal": recordsTotal,
  };
}

class NotificationItem {
  NotificationItem({
    this.title,
    this.description,
    this.content,
    this.slug,
    this.status,
    this.createdAt,
    this.updatedAt,
  });

  final String title;
  final String description;
  final String content;
  final String slug;
  final int status;
  final DateTime createdAt;
  final DateTime updatedAt;

  factory NotificationItem.fromRawJson(String str) => NotificationItem.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory NotificationItem.fromJson(Map<String, dynamic> json) => NotificationItem(
    title: json["title"],
    description: json["description"],
    content: json["content"],
    slug: json["slug"],
    status: json["status"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "title": title,
    "description": description,
    "content": content,
    "slug": slug,
    "status": status,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
  };
}
