import 'dart:convert';

class Commitment {
  Commitment({
    this.status,
    this.data,
    this.message,
  });

  final int status;
  final CommitmentData data;
  final String message;

  factory Commitment.fromRawJson(String str) => Commitment.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Commitment.fromJson(Map<String, dynamic> json) => Commitment(
    status: json["status"],
    data: CommitmentData.fromJson(json["data"]),
    message: json["message"],
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "data": data.toJson(),
    "message": message,
  };
}

class CommitmentData {
  CommitmentData({
    this.rows,
  });

  final List<CommitmentItem> rows;

  factory CommitmentData.fromRawJson(String str) => CommitmentData.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory CommitmentData.fromJson(Map<String, dynamic> json) => CommitmentData(
    rows: List<CommitmentItem>.from(json["rows"].map((x) => CommitmentItem.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "rows": List<dynamic>.from(rows.map((x) => x.toJson())),
  };
}

class CommitmentItem {
  CommitmentItem({
    this.id,
    this.shortTitle,
    this.title,
    this.description,
    this.displayOrder,
    this.createdAt,
    this.updatedAt,
  });

  final int id;
  final String shortTitle;
  final String title;
  final String description;
  final int displayOrder;
  final DateTime createdAt;
  final DateTime updatedAt;

  factory CommitmentItem.fromRawJson(String str) => CommitmentItem.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory CommitmentItem.fromJson(Map<String, dynamic> json) => CommitmentItem(
    id: json["id"],
    shortTitle: json["short_title"],
    title: json["title"],
    description: json["description"],
    displayOrder: json["display_order"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "short_title": shortTitle,
    "title": title,
    "description": description,
    "display_order": displayOrder,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
  };
}
