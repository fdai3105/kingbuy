// To parse this JSON data, do
//
//     final disctrict = disctrictFromJson(jsonString);

import 'dart:convert';

class District {
  District({
    this.status,
    this.data,
  });

  final int status;
   List<DistrictItem> data;

  factory District.fromRawJson(String str) => District.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory District.fromJson(Map<String, dynamic> json) => District(
    status: json["status"],
    data: List<DistrictItem>.from(json["data"].map((x) => DistrictItem.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class DistrictItem {
  DistrictItem({
    this.code,
    this.nameWithType,
    this.deliveryStatus,
    this.shipFeeBulky,
    this.shipFeeNotBulky,
  });

  final String code;
  final String nameWithType;
  final List<int> deliveryStatus;
  final int shipFeeBulky;
  final int shipFeeNotBulky;

  factory DistrictItem.fromRawJson(String str) => DistrictItem.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory DistrictItem.fromJson(Map<String, dynamic> json) => DistrictItem(
    code: json["code"],
    nameWithType: json["name_with_type"],
    deliveryStatus: List<int>.from(json["delivery_status"].map((x) => x)),
    shipFeeBulky: json["ship_fee_bulky"],
    shipFeeNotBulky: json["ship_fee_not_bulky"],
  );

  Map<String, dynamic> toJson() => {
    "code": code,
    "name_with_type": nameWithType,
    "delivery_status": List<dynamic>.from(deliveryStatus.map((x) => x)),
    "ship_fee_bulky": shipFeeBulky,
    "ship_fee_not_bulky": shipFeeNotBulky,
  };

  @override
  String toString() {
    return 'DistrictItem{code: $code, nameWithType: $nameWithType, deliveryStatus: $deliveryStatus, shipFeeBulky: $shipFeeBulky, shipFeeNotBulky: $shipFeeNotBulky}';
  }
}
