class UserAddress {
  String fullName;
  String phoneNumber;
  String phoneNumberSecond;
  String city;
  String district;
  String ward;
  String address;

  UserAddress({
    this.fullName,
    this.phoneNumber,
    this.phoneNumberSecond,
    this.city,
    this.district,
    this.ward,
    this.address,
  });
}
