import 'dart:convert';

class MyPromotion {
  MyPromotion({
    this.status,
    this.data,
  });

  final int status;
  final MyPromotionData data;

  factory MyPromotion.fromRawJson(String str) => MyPromotion.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory MyPromotion.fromJson(Map<String, dynamic> json) => MyPromotion(
    status: json["status"],
    data: MyPromotionData.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "data": data.toJson(),
  };
}

class MyPromotionData {
  MyPromotionData({
    this.promotions,
    this.recordsTotal,
  });

  final List<MyPromotionItem> promotions;
  final int recordsTotal;

  factory MyPromotionData.fromRawJson(String str) => MyPromotionData.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory MyPromotionData.fromJson(Map<String, dynamic> json) => MyPromotionData(
    promotions: List<MyPromotionItem>.from(json["promotions"].map((x) => MyPromotionItem.fromJson(x))),
    recordsTotal: json["recordsTotal"],
  );

  Map<String, dynamic> toJson() => {
    "promotions": List<dynamic>.from(promotions.map((x) => x.toJson())),
    "recordsTotal": recordsTotal,
  };
}

class MyPromotionItem {
  MyPromotionItem({
    this.id,
    this.title,
    this.imageSource,
    this.description,
    this.content,
    this.slug,
    this.status,
    this.createdAt,
    this.updatedAt,
  });

  final int id;
  final String title;
  final String imageSource;
  final String description;
  final String content;
  final String slug;
  final int status;
  final DateTime createdAt;
  final DateTime updatedAt;

  factory MyPromotionItem.fromRawJson(String str) => MyPromotionItem.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory MyPromotionItem.fromJson(Map<String, dynamic> json) => MyPromotionItem(
    id: json["id"],
    title: json["title"],
    imageSource: json["image_source"],
    description: json["description"],
    content: json["content"],
    slug: json["slug"],
    status: json["status"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "title": title,
    "image_source": imageSource,
    "description": description,
    "content": content,
    "slug": slug,
    "status": status,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
  };
}