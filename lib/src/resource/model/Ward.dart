import 'dart:convert';

class Ward {
  Ward({
    this.status,
    this.data,
  });

  final int status;
  List<WardItem> data;

  factory Ward.fromRawJson(String str) => Ward.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Ward.fromJson(Map<String, dynamic> json) => Ward(
    status: json["status"],
    data: List<WardItem>.from(json["data"].map((x) => WardItem.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class WardItem {
  WardItem({
    this.code,
    this.nameWithType,
  });

  final String code;
  final String nameWithType;

  factory WardItem.fromRawJson(String str) => WardItem.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory WardItem.fromJson(Map<String, dynamic> json) => WardItem(
    code: json["code"],
    nameWithType: json["name_with_type"],
  );

  Map<String, dynamic> toJson() => {
    "code": code,
    "name_with_type": nameWithType,
  };
}
