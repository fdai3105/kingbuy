import 'dart:convert';

class Rating {
  Rating({
    this.status,
    this.data,
    this.message,
  });

  final int status;
  final RatingItem data;
  final String message;

  factory Rating.fromRawJson(String str) => Rating.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Rating.fromJson(Map<String, dynamic> json) => Rating(
        status: json["status"],
        data: RatingItem.fromJson(json["data"]),
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "data": data.toJson(),
        "message": message,
      };
}

class RatingItem {
  RatingItem({
    this.oneStarCount,
    this.twoStarCount,
    this.threeStarCount,
    this.fourStarCount,
    this.fiveStarCount,
    this.avgRating,
    this.ratingCount,
  });

  final int oneStarCount;
  final int twoStarCount;
  final int threeStarCount;
  final int fourStarCount;
  final int fiveStarCount;
  final int avgRating;
  final int ratingCount;

  factory RatingItem.fromRawJson(String str) => RatingItem.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory RatingItem.fromJson(Map<String, dynamic> json) => RatingItem(
        oneStarCount: json["one_star_count"],
        twoStarCount: json["two_star_count"],
        threeStarCount: json["three_star_count"],
        fourStarCount: json["four_star_count"],
        fiveStarCount: json["five_star_count"],
        avgRating: json["avg_rating"],
        ratingCount: json["rating_count"],
      );

  Map<String, dynamic> toJson() => {
        "one_star_count": oneStarCount,
        "two_star_count": twoStarCount,
        "three_star_count": threeStarCount,
        "four_star_count": fourStarCount,
        "five_star_count": fiveStarCount,
        "avg_rating": avgRating,
        "rating_count": ratingCount,
      };
}
