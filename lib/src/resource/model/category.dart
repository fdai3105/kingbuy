import 'dart:convert';

class Category {
  Category({
    this.status,
    this.data,
  });

  final int status;
  final CategoryData data;

  factory Category.fromRawJson(String str) =>
      Category.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Category.fromJson(Map<String, dynamic> json) => Category(
        status: json["status"],
        data: CategoryData.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "data": data.toJson(),
      };
}

class CategoryData {
  CategoryData({
    this.categories,
    this.recordsTotal,
  });

  final List<CategoryItem> categories;
  final int recordsTotal;

  factory CategoryData.fromRawJson(String str) =>
      CategoryData.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory CategoryData.fromJson(Map<String, dynamic> json) => CategoryData(
        categories: List<CategoryItem>.from(
            json["categories"].map((x) => CategoryItem.fromJson(x))),
        recordsTotal: json["recordsTotal"],
      );

  Map<String, dynamic> toJson() => {
        "categories": List<dynamic>.from(categories.map((x) => x.toJson())),
        "recordsTotal": recordsTotal,
      };
}

class CategoryItem {
  CategoryItem({
    this.id,
    this.name,
    this.imageSource,
    this.iconSource,
    this.backgroundImage,
    this.parentId,
    this.parent,
    this.children,
    this.description,
    this.slug,
    this.videoLink,
  });

  final int id;
  final String name;
  final String imageSource;
  final String iconSource;
  final String backgroundImage;
  final int parentId;
  final CategoryParent parent;
  final List<CategoryItem> children;
  final dynamic description;
  final String slug;
  final String videoLink;

  factory CategoryItem.fromRawJson(String str) =>
      CategoryItem.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory CategoryItem.fromJson(Map<String, dynamic> json) => CategoryItem(
        id: json["id"],
        name: json["name"],
        imageSource: json["image_source"],
        iconSource: json["icon_source"] == null ? null : json["icon_source"],
        backgroundImage:
            json["background_image"] == null ? null : json["background_image"],
        parentId: json["parent_id"] == null ? null : json["parent_id"],
        parent: json["parent"] == null
            ? null
            : CategoryParent.fromJson(json["parent"]),
        children: List<CategoryItem>.from(
            json["children"].map((x) => CategoryItem.fromJson(x))),
        description: json["description"],
        slug: json["slug"],
        videoLink: json["video_link"] == null ? null : json["video_link"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "image_source": imageSource,
        "icon_source": iconSource == null ? null : iconSource,
        "background_image": backgroundImage == null ? null : backgroundImage,
        "parent_id": parentId == null ? null : parentId,
        "parent": parent == null ? null : parent.toJson(),
        "children": List<dynamic>.from(children.map((x) => x.toJson())),
        "description": description,
        "slug": slug,
        "video_link": videoLink == null ? null : videoLink,
      };

  @override
  String toString() {
    return 'ChildCategory{id: $id, name: $name, imageSource: $imageSource, iconSource: $iconSource, backgroundImage: $backgroundImage, parentId: $parentId, parent: $parent, children: $children, description: $description, slug: $slug, videoLink: $videoLink}';
  }
}

class CategoryParent {
  CategoryParent({
    this.id,
    this.name,
    this.slug,
    this.videoLink,
    this.parent,
  });

  final int id;
  final String name;
  final String slug;
  final String videoLink;
  final dynamic parent;

  factory CategoryParent.fromRawJson(String str) =>
      CategoryParent.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory CategoryParent.fromJson(Map<String, dynamic> json) => CategoryParent(
        id: json["id"],
        name: json["name"],
        slug: json["slug"],
        videoLink: json["video_link"],
        parent: json["parent"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "slug": slug,
        "video_link": videoLink,
        "parent": parent,
      };

  @override
  String toString() {
    return '_Parent{id: $id, name: $name, slug: $slug, videoLink: $videoLink, parent: $parent}';
  }
}
