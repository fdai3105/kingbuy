import 'dart:convert';

import 'package:hues_kingbuy/src/resource/model/product.dart';

class Cart {
  final ProductItem product;
  int quantity;

  Cart({this.product, this.quantity});

  String toRawJson() => json.encode(toJson());

  Map<String, dynamic> toJson() => {
        "quantity": quantity,
        "item": product.toJson(),
      };

  factory Cart.fromRawJson(String str) => Cart.fromJson(json.decode(str));

  factory Cart.fromJson(Map<String, dynamic> map) => Cart(
        quantity: map['quantity'],
        product: ProductItem.fromJson(map['item']),
      );

  @override
  String toString() {
    return 'Cart{products: $product, quantity: $quantity}';
  }
}
