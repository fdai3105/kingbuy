import 'package:dio/dio.dart';
import 'package:dio/native_imp.dart';
import 'package:hues_kingbuy/src/config/config.dart';

class DioService extends DioForNative {
  DioService({String baseUrl = AppEndpoint.BASE, BaseOptions options})
      : super(options) {
    this.interceptors.add(
          InterceptorsWrapper(
            onRequest: _request,
            onResponse: _response,
            onError: _error,
          ),
        );
    this.options.baseUrl = baseUrl;
  }

  _request(RequestOptions options) {
    options.connectTimeout = 10000;
    options.receiveTimeout = 10000;
    return options;
  }

  _response(Response response) {
    return response;
  }

  _error(DioError error) {
    return error;
  }
}
