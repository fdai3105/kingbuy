import 'package:flutter/material.dart';
import 'package:hues_kingbuy/src/screen/address/address_add_screen.dart';
import 'package:hues_kingbuy/src/screen/address/address_screen.dart';
import 'package:hues_kingbuy/src/screen/bando/bando.dart';
import 'package:hues_kingbuy/src/screen/cart/cart.dart';
import 'package:hues_kingbuy/src/screen/cart_coupon/cart_coupon_screen.dart';
import 'package:hues_kingbuy/src/screen/category/category.dart';
import 'package:hues_kingbuy/src/screen/dashboard/dashboard_screen.dart';
import 'package:hues_kingbuy/src/screen/dashboard/user/commitment/commitment_screen.dart';
import 'package:hues_kingbuy/src/screen/dashboard/user/contact/contact_screen.dart';
import 'package:hues_kingbuy/src/screen/dashboard/user/khuyenmai/khuyenmai_detail_screen.dart';
import 'package:hues_kingbuy/src/screen/dashboard/user/khuyenmai/khuyenmai_screen.dart';
import 'package:hues_kingbuy/src/screen/detail_coupon/detail_coupon_screen.dart';
import 'package:hues_kingbuy/src/screen/login/login.dart';
import 'package:hues_kingbuy/src/screen/product/product.dart';
import 'package:hues_kingbuy/src/screen/search/search_screen.dart';

class Routes {
  static const String loginScreen = 'login_screen';
  static const String signupScreen = 'signup_screen';
  static const String dashboardScreen = 'dashboard_screen';
  static const String searchScreen = "search_screen";
  static const String promotionScreen = "promotion_screen";
  static const String promotionDetailScreen = "promotionDetail_screen";
  static const String commitmentScreen = "commitment_screen";
  static const String contactScreen = "contact_screen";
  static const String categoryScreen = "category_screen";
  static const String productScreen = "product_screen";
  static const String detailCoupon = "detail_coupon";
  static const String bandoScreen = "bando_screen";
  static const String cartScreen = "cart_screen";
  static const String addressScreen = "address_screen";
  static const String addressAddScreen = "address_add_screen";
  static const String cartCouponScreen = "cart_coupon_screen";

  static final Map<String, WidgetBuilder> routes = {
    loginScreen: (_) => LoginScreen(),
    dashboardScreen: (_) => DashBoardScreen(),
    searchScreen: (_) => SearchScreen(),
    promotionScreen: (_) => KhuyenMaiScreen(),
    promotionDetailScreen: (_) => KhuyenMaiDetailScreen(),
    commitmentScreen: (_) => CommitmentScreen(),
    contactScreen: (_) => ContactScreen(),
    categoryScreen: (_) => CategoryScreen(),
    productScreen: (_) => ProductScreen(),
    detailCoupon: (_) => DetailCouponScreen(),
    bandoScreen: (_) => BandoScreen(),

    /// -------- cart
    cartScreen: (_) => CartScreen(),
    addressScreen: (_) => AddressScreen(),
    addressAddScreen: (_) => AddressAddScreen(),
    cartCouponScreen: (_) => CartCouponScreen()

    /// --------
  };
}
