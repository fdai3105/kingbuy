import 'package:intl/intl.dart';

class CustomFormat {
  static String formatMoney(dynamic text) {
    final format = NumberFormat();
    return "${format.format(text)}₫";
  }

  static String formatDate(DateTime datetime) {
    final result = DateFormat("dd-MM-y").format(datetime);
    return result;
  }
}
