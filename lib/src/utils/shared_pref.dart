import 'dart:convert';
import 'dart:io';

import 'package:hues_kingbuy/src/resource/model/model.dart';
import 'package:hues_kingbuy/src/resource/model/product.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPref {
  static SharedPreferences _pref;

  static Future getInstance() async {
    _pref = await SharedPreferences.getInstance();
  }

  static Future saveUser(User user) async {
    await _pref.setInt("id", user.data.profile.id);
    await _pref.setString("name", user.data.profile.name);
    await _pref.setString("email", user.data.profile.email);
    await _pref.setString("phone", user.data.profile.phoneNumber);
    await _pref.setString("avatar", user.data.profile.avatarSource);
    await _pref.setString("token", user.data.token);
    await _pref.setInt("point", user.data.rewardPoints);
    await _pref.setString("cardNumber", user.data.memberCardNumber);
  }

  static User getUser() {
    final user = User(
      status: 1,
      message: "form shared pref",
      data: UserData(
        memberCardNumber: _pref.get("cardNumber"),
        rewardPoints: _pref.get("point"),
        token: _pref.get("token"),
        profile: UserItem(
          id: _pref.get("id"),
          name: _pref.get("name"),
          email: _pref.get("email"),
          phoneNumber: _pref.get("phone"),
          avatarSource: _pref.get("avatar"),
        ),
      ),
    );
    return user;
  }

  static String getToken() {
    return _pref.get("token");
  }

  static Future saveCarts(List<Cart> carts) async {
    await _pref.remove('carts');
    final List<String> convert = carts.map((e) {
      return e.toRawJson();
    }).toList();
    await _pref.setStringList("carts", convert);
  }

  static List<Cart> getCarts() {
    final list = <Cart>[];
    final cartRaw = _pref.getStringList('carts');
    if (cartRaw != null) {
      for (final cart in cartRaw) {
        list.add(Cart.fromRawJson(cart));
      }
    }
    return list;
  }

  static Future clear() async {
    await _pref.clear();
  }
}
