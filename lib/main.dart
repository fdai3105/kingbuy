import 'package:flutter/material.dart';
import 'package:hues_kingbuy/src/resource/model/category.dart';
import 'package:hues_kingbuy/src/screen/address/address_screen.dart';
import 'package:hues_kingbuy/src/screen/address/address_viewmodel.dart';
import 'package:hues_kingbuy/src/screen/cart/cart.dart';
import 'package:hues_kingbuy/src/screen/category/category.dart';
import 'package:hues_kingbuy/src/screen/dashboard/dashboard_screen.dart';
import 'package:hues_kingbuy/src/screen/login/login.dart';
import 'package:hues_kingbuy/src/utils/shared_pref.dart';
import 'package:hues_kingbuy/src/utils/utils.dart';
import 'package:provider/provider.dart';

import 'src/resource/repository/repository.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SharedPref.getInstance();

  runApp(
    MultiProvider(providers: [
      ChangeNotifierProvider<LoginViewmodel>.value(
        value: LoginViewmodel(UserRepository()),
        child: LoginScreen(),
      ),
      ChangeNotifierProvider<CartViewmodel>.value(
        value: CartViewmodel(ProductRepository())..init(),
        child: CartScreen(),
      ),
      ChangeNotifierProvider<AddressViewmodel>.value(
        value: AddressViewmodel(AddressRepository())..init(),
        child: AddressScreen(),
      ),
      ChangeNotifierProvider<CategoryViewmodel>.value(
        value: CategoryViewmodel(ProductRepository()),
        child: CategoryScreen(),
      ),
    ], child: MyApp()),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: DashBoardScreen(),
      routes: Routes.routes,
      debugShowCheckedModeBanner: false,
    );
  }
}
